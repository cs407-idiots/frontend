## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## How To:

#### Change Testbed Server Hostname

To change the hostname of the backend testbed server, access `constants/endpoints.js`
 and set the `hostName` constant to the desired address.

#### Add/Alter Endpoint Constants

 To amend the preloaded endpoint constants that the UI sends/receives data to/from, access `constants/endpoints.js`
  and set the desired constant to the correct path, 
  or add a new constant that follows the same template as those that are preloaded.

#### Add/Alter Image OS/Platform Options

On the UploadImage page, there is a form for the user to fill out with meta data about their uploaded image, 
which is sourced from `components/UploadImage.js`. To change the user's options for the Image OS and/or platform,
access `constants/index.js` and make changes to either `image_platform` or `image_os` arrays. <br />

**Ensure** the display name of the platform/image is entered into the `value` field, and the identifier to send to
the testbed server is entered into the `key` field.

#### Add/Alter Image Accepted File Types

On the UploadImage page, the user is limited in what kinds of files they are able to upload. 
These valid files are displayed to the user within the dropzone, and also enforced by the file selector control. 
To change which file types are accepted for sending to the testbed server, access `constants/index.js` and make 
the relevant change to either `accepted_image_file_types`.

#### Alter Mote Connection Timeout Status

In the interactive test configurator, the user is able to select checkboxes of motes that they wish to use in their test.
While all motes are displayed from the testbed server, any motes with a Connection Timeout status message are given a `disabled`
checkbox.

In order the change the corresponding string of this status message, access `constants/index.js` and make
the relevant change to `mote_timeout_status`.

#### Alter CCA Bar Graph Colour

On the Mote Availability page, within LucidLab metrics, the user can view bar charts to assess the CCA
score across both individual motes and the global system. By default, the colour of these bars is set to the
same HEX code as the green used in the body of the Navbar component, to maintain sitewide consistency. 

If you wish to change the colour of these bars, access `constants/index.js` and make the relevant change 
to `cca_bar_chart_colour`.

#### Add/Alter Component Styles

`React` and the chosen UI framework `Material-UI` allow for styles to be specified in the `.js` file of a component.

Documentation for styling with `Material-UI`: [Basic](https://material-ui.com/styles/basics/), [Advanced](https://material-ui.com/styles/advanced/), [API](https://material-ui.com/styles/api/)



Styles can be set with the an anonymous function called `useStyles`. The following is an example of how to add a style called `root` which applies a top margin of `2em`. Additional styles can be added, removed and changed inside the function.

```
const useStyles = theme => ({
  root: {
    marginTop: "2em",
  }
});
```



Next, in order for styling to be applied to a component, the `useStyles` property must be passed to the custom component using `withStyles`. This allows the `classes` property to contain all the styling properties which allows for individual `DOM` elements to be styled. Here is a basic structure for how to implement styles in a higher-order component.

```
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = theme => ({
  root: {
    border: "1px solid red",
  }
});

class CustomComponent extends React.Component {
  constructor(props) {
    super(props);
    ...
  }
  
  render() {
    const { classes } = this.props;
    return (
      ...
    );
  }
  
  ...
}

# Ensure styling is passes to the object.
CustomComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(useStyles)(CustomComponent);
```



Finally, to apply a style in the `render` method to a specific `DOM` element, use the `className` attribute:

```
<p className={classes.root}>Hello</p>
```



#### Add/Alter Global Styles

To apply global styles to all components, or to alter existing global styling, access the file `src/App.css`. Please note that ideally, one should refrain from adding too much styling to this file and instead opt to distribute styles to their relevant component wherever possible.