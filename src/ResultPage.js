/*
    ResultPage.js

    Displays the results of the user's jobs, by querying the endpoint which looks for jobs associated
    to the current user, with a completed flag=true.

    Performs an asynchronous GET request for the results, in order to populate the table with data from
    both the GetCompletedJobs and GetConfigInfo endpoints.

    Once populated, the table gives the user the options on each row to either download the configuration
    that was run to create the result, download the result in CSV format, or delete the result from the
    system.
 */

import React from 'react';
import './App.css';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import MaterialTable from "material-table";
import EndpointSnackbar from "./components/EndpointSnackbar";
import Endpoint from './Endpoint';

import * as endpoints from './constants/endpoints';

const useStyles = ({
    root: {
        marginTop: "5em",
        marginBottom: "5em",
    },
    title: {
        fontSize: "2rem",
        textAlign: "left",
        fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif",
    },
    text: {
        fontSize: "1rem",
        textAlign: "left",
        marginTop: "2em",
        marginBottom: "2em",
    },
    divider: {
        marginTop: "4em",
        marginBottom: "4em",
        color: "#4BA173",
    },
});

class ResultPage extends React.Component {
    jobs;
    test_name;
    test_description;
    test_duration;
    scheduled_start_time;

    constructor(props) {
        super(props);
        this.child = React.createRef();
        this.state = {
            table: [],
            jobs: []
        };
    }
    componentDidMount() {
        this.fetchJobs();
    }

    // Fetches the jobs from the endpoint.
    fetchJobs = async () => {
        const userID = this.props.userID;
        const data = {"user_id": userID};

        const fetchJobsEndpoint = new Endpoint(
            endpoints.GetCompletedJobs,
            this.loadJobsSuccessCallback.bind(this),
            this.errorCallback.bind(this)
        );
        await fetchJobsEndpoint.asyncGet(data);
    };

    // Fetches the jobs from the endpoint.
    fetchConfigInfo = async (configID) => {
        const data = {"test_id": configID};

        const fetchConfigInfoEndpoint = new Endpoint(
            endpoints.GetConfigInfo,
            this.loadConfigInfoSuccessCallback.bind(this),
            this.errorCallback.bind(this)
        );

        let endpointData = {};

        await fetchConfigInfoEndpoint.asyncGet(data).then(function(data) {
            endpointData = data;
        });

        return endpointData;
    };

    // Download a config from the table.
    downloadConfig = async (rowData) => {
        let response = {};

        const jobID = rowData.id;

        const data = {"job_id": jobID};
    
        const downloadConfigInfoEndpoint = new Endpoint(
            endpoints.DownloadConfig,
            this.downloadConfigInfoSuccessCallback.bind(this),
            this.errorCallback.bind(this)
        );
    
        await downloadConfigInfoEndpoint.asyncGet(data).then(function(res) {
            response = res;
        });

        const headers = response.headers;
        const yamlObj = response.data;

        // Extracts the filename from the content-disposition header
        const filename = headers['content-disposition'].split("=")[1];

        // Creates a file by converting the python object into a Blob
        const file = new Blob([yamlObj], {type: 'text/yaml'});
        // Creates an anchor node for the duration of a download
        // Allows the specification of a filename, set to iot_controller.py
        // Reliably starts the download automatically
        let downloadAnchorNode = document.createElement('a');
        // Creates an object URL with which to reference the file on the site
        downloadAnchorNode.setAttribute("href", URL.createObjectURL(file));
        downloadAnchorNode.setAttribute("download",  filename);
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    };

    // Download a result in the table from the database.
    downloadResult = async (rowData) => {
        const job_id = rowData.id;

        const data = {"job_id": job_id};

        const downloadResultEndpoint = new Endpoint(
            endpoints.DownloadResult,
            this.downloadResultSuccessCallback.bind(this),
            this.errorCallback.bind(this)
        );

        let csvObj = {};

        await downloadResultEndpoint.asyncGet(data).then(function(data) {
            csvObj = data;
        });

        // Creates a data string and includes the relevant JSON Object
        const dataStr = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvObj);
        // Creates an anchor node for the duration of a download
        // Allows the specification of a filename, set to config.json
        // Reliably starts the download automatically
        let downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute("href", dataStr);
        downloadAnchorNode.setAttribute("download",  "results.csv");
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    };

    // Removes a result in the table.
    removeResult(rowData) {
        // eslint-disable-next-line no-restricted-globals
        if (confirm("Are you sure you want to delete this result?")) {
            const job_id = parseInt(rowData.id);
            const data = {"job_id": job_id};

            const removeJobEndpoint = new Endpoint(
                endpoints.DeleteJob,
                this.removeSuccessCallback.bind(this),
                this.errorCallback.bind(this)
            );
            removeJobEndpoint.get(data);

            const index = rowData.tableData.id;
            let table = this.state.table;
            table.splice(index, 1);
            this.setState({table});
        }
    }

    // Load Jobs Endpoint success callback.
    loadJobsSuccessCallback = async (response) => {
        const jobs = response.data.jobs;

        this.setState({
            jobs: jobs
        });

        // Map over the retrieved jobs, and for each retrieve the associated config data to properly
        // populate the table with name, description and duration
        let tableData = jobs.map(async job =>  {

            let configData = {};
            await this.fetchConfigInfo(job.test_id).then(function(data) {
                configData = data;
            }, function(error) {
                console.error("Failed!", error);
            });

            return {
                id: job.job_id,
                title: configData.name,
                desc: configData.desc,
                start: new Date(job.scheduled_start_time),
                duration: configData.duration
            };
        });

        Promise.all(tableData)
            .then(results => {
                this.setState({
                    table: results
                })
            })
            .catch(e => {
                console.error("Error: ", e);
            });
    };

    // Load Config Info Endpoint success callback.
    loadConfigInfoSuccessCallback = async (response) => {
        const configInfo = response.data;
        return {
            id: configInfo.test_id,
            name: configInfo.test_name,
            desc: configInfo.test_description,
            duration: configInfo.test_duration
        };
    };

    // Load Config File success callback.
    downloadConfigInfoSuccessCallback = async (response) => {
        return response;
    };

    // Load Result success callback.
    downloadResultSuccessCallback = async (response) => {
        return response.data;
    };

    // Remove Endpoint success callback.
    removeSuccessCallback(response) {
        const removeSuccessMsg = "Successfully Deleted Result.";
        this.child.current.customMessage("success", removeSuccessMsg);
    }

    // Endpoint error callback.
    errorCallback(error, msg) {
        this.child.current.customMessage("error", msg);
    }

    render () {
        const { classes } = this.props;

        return (
            <Container className={classes.root} maxWidth="lg">
                <h1 className={classes.title}>Test Results</h1>

                <MaterialTable
                    columns={[
                        { title: 'ID', field: 'id', type: 'numeric', defaultSort: 'desc'},
                        { title: 'Title', field: 'title'},
                        { title: 'Description', field: 'desc', sorting: false},
                        { title: 'Start Time', field: 'start', type: 'datetime'},
                        { title: 'Duration (s)', field: 'duration', type: 'numeric'},

                    ]}
                    data={this.state.table}
                    actions={[
                        {
                            icon: 'system_update_alt_icon',
                            tooltip: 'Download Configuration',
                            onClick: (event, rowData) => this.downloadConfig(rowData)
                        },
                        {
                            icon: 'get_app',
                            tooltip: 'Download Result',
                            onClick: (event, rowData) => this.downloadResult(rowData)
                        },
                        {
                            icon: 'delete',
                            tooltip: 'Delete Result',
                            onClick: (event, rowData) => this.removeResult(rowData)
                        }
                    ]}
                    options={{
                        sorting: true,
                        search: false,
                        showTitle: false,
                        actionsColumnIndex: -1
                    }}
                />

                <EndpointSnackbar ref={this.child} />
            </Container>
        );
    }
}

ResultPage.propTypes = {
    classes: PropTypes.object.isRequired,
    userID: PropTypes.number.isRequired
};

export default withStyles(useStyles)(ResultPage)
