/*
    src/index.js

    This file acts as the root of the site, and handles the storing of session data in the client-
    side cookies to manage the signed in state.
    It also handles the routing of the application, by linking user readable paths to their respective
    components within the codebase.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import NavBar from './components/NavigationBar';
import Footer from './components/Footer';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import { createMuiTheme, ThemeProvider, responsiveFontSizes } from '@material-ui/core/styles';
import Cookies from 'js-cookie';
// Website pages.
import HomePage from './HomePage';
import TestPage from './TestPage';
import UploadsPage from './UploadsPage';
import ResultPage from './ResultPage';
import StatusPage from './StatusPage';
import NotFound from './NotFound';
import NotAuthorised from './NotAuthorised';

//Global Themes.
const theme = responsiveFontSizes(createMuiTheme({
  typography: {
    h1: { fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif" },
    h2: { fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif" },
    h3: { fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif" },
    h4: { fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif" },
    h5: { fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif" },
    h6: { fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif" },
  },
}));


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getSessionState();
  }

  // Initial session data.
  getSessionState() {
    let cookieUserID = Cookies.get("userID"); 
    let cookieSignedIn = Cookies.get("signedIn");

    cookieUserID = (cookieUserID === undefined) ? undefined : parseInt(cookieUserID);
    cookieSignedIn = (cookieSignedIn === undefined) ? false : true;
  
    return {
      userID: cookieUserID,
      signedIn: cookieSignedIn,
    };
  }

  // Sets the local session data.
  updateSessionState(signedIn, userID) {
    this.setState({ signedIn: signedIn, userID: userID });

    const expiry60Mins = new Date(new Date().getTime() + 60 * 60 * 1000);

    if (signedIn) {
      // Cookies are automatically removes after 30 minutes.
      Cookies.set("userID", userID, { expires: expiry60Mins, sameSite: "strict" });
      Cookies.set("signedIn", signedIn, { expires: expiry60Mins, sameSite: "strict" });
    }
    else {
      Cookies.remove("userID");
      Cookies.remove("signedIn");
    }
  }
  

  // User Authentication.
  isLoggedIn() {
    return this.state.signedIn;
  }

  render() {
    return (
      <BrowserRouter >
        <React.Fragment>
        <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="App">
          <NavBar title="LucidLab" signedIn={this.state.signedIn} signedInChange={this.updateSessionState.bind(this)} />
          <main>
            <Switch>
              <Route exact path='/' component={HomePage} />
              <Route path="/401" render={() => <NotAuthorised signedIn={this.state.signedIn} />} />
              <Route path="/tests" render={() => (
                !this.isLoggedIn() ? (<Redirect to="/401" />) : (<TestPage userID={this.state.userID} />)
              )} />
              <Route path="/uploads" render={() => (
              !this.isLoggedIn() ? (<Redirect to="/401" />) : (<UploadsPage userID={this.state.userID}/>)
              )} />
              <Route path="/results" render={() => (
              !this.isLoggedIn() ? (<Redirect to="/401" />) : (<ResultPage userID={this.state.userID}/>)
              )} />
              <Route path="/metrics" render={() => (
              !this.isLoggedIn() ? (<Redirect to="/401" />) : (<StatusPage userID={this.state.userID} />)
              )} />
              
              <Route render={() => <NotFound signedIn={this.state.signedIn} />} />
            </Switch>
          </main>
          <Footer />
        </div>
        </ThemeProvider>
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
