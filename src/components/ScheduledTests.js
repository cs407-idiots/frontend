/*
    ScheduledTests.js

    This page is used to list the jobs currently scheduled on the tested, for all users. Rather than
    limit the table to just the current user's jobs, we decided it was best to show them everyone's jobs
    so that they can get a better idea of the current workload of the testbed. The table gives the
    user options to cancel the job from both the table and the back-end server.

    It retrieves the predefined padding from the back-end server, for adding to the duration and start
    time to attain an estimated end time of the test.
 */

import React from 'react';
import '../App.css';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import MaterialTable from "material-table";
import Endpoint from "../Endpoint";
import EndpointSnackbar from './EndpointSnackbar';
import * as endpoints from "../constants/endpoints";

const useStyles = ({
    root: {
        '& p': {
            marginBottom: '-25px'
        }
    },
    title: {
        marginTop: "1em",
        marginBottom: "1em",
    },
});

class ScheduledTests extends React.Component {
  constructor(props) {
      super(props);
      this.child = React.createRef();
      this.state = {
        table: [],
      };
  }

    componentDidMount() {
        this.fetchScheduledJobs();
    }

    // Fetches the scheduled jobs from the endpoint.
    fetchScheduledJobs = async () => {
        // Gets all non completed jobs for all users
        const data = {"completed": false};

        const loadScheduledJobsEndpoint = new Endpoint(
            endpoints.GetAllJobs,
            this.loadSuccessCallback.bind(this),
            this.errorCallback.bind(this)
        );

        await loadScheduledJobsEndpoint.asyncGet(data);
    };

    // Fetches the jobs from the endpoint.
    fetchConfigInfo = async (configID) => {
        const data = {"test_id": configID};

        const fetchConfigInfoEndpoint = new Endpoint(
            endpoints.GetConfigInfo,
            this.loadConfigInfoSuccessCallback.bind(this),
            this.errorCallback.bind(this)
        );

        let endpointData = {};

        await fetchConfigInfoEndpoint.asyncGet(data).then(function(data) {
            endpointData = data;
        });

        return endpointData;
    };

  // Cancels a test.
  cancelJob(rowData) {
      if (this.props.userID === rowData.user_id) {
          // eslint-disable-next-line no-restricted-globals
          if (confirm("Are you sure you want to delete this scheduled test?")) {
              const job_id = parseInt(rowData.id);
              const data = {"job_id": job_id};

              const cancelJobEndpoint = new Endpoint(
                  endpoints.DeleteJob,
                  this.removeSuccessCallback.bind(this),
                  this.errorCallback.bind(this)
              );
              cancelJobEndpoint.get(data);

              const index = rowData.tableData.id;
              let table = this.state.table;
              table.splice(index, 1);
              this.setState({table});
          }
      } else {
          alert("You cannot delete a test that you do not own.");
      }
  }

    // Load Endpoint success callback.
    loadSuccessCallback(response) {
        const jobs = response.data.jobs;
        this.setState({
            jobs: jobs
        });

        let tableData = jobs.map(async job =>  {
            let configData = {};
            await this.fetchConfigInfo(job.test_id).then(function(data) {
                configData = data;
            }, function(error) {
                console.error("Failed!", error);
            });

            const startTime = new Date(job.scheduled_start_time);
            const padding = job.padding;
            const endTime = new Date(
                startTime.setSeconds( startTime.getSeconds()
                + configData.duration
                + padding
            ));

            return { id: job.job_id,
                title: configData.name,
                desc: configData.desc,
                start: new Date(job.scheduled_start_time),
                end: endTime,
                duration: configData.duration,
                user_id: job.user_id };
        });

        Promise.all(tableData)
            .then(results => {
                this.setState({
                    table: results
                })
            })
            .catch(e => {
                console.error("Error: ", e);
            });
    }

    // Load Config Info Endpoint success callback.
    loadConfigInfoSuccessCallback = async (response) => {
        const configInfo = response.data;
        return {
            id: configInfo.test_id,
            name: configInfo.test_name,
            desc: configInfo.test_description,
            duration: configInfo.test_duration
        };
    };

    // Remove Endpoint success callback.
    removeSuccessCallback(response) {
        const removeSuccessMsg = "Successfully Cancelled Test.";
        this.child.current.customMessage("success", removeSuccessMsg);
    }

    // Endpoint error callback.
    errorCallback(error, msg) {
        this.child.current.customMessage("error", msg);
    }

  render() {
    const { classes } = this.props;

    return (
      <Container className={classes.root}>
          <Typography className={classes.title} variant="h4" component="h1">Scheduled Tests</Typography>
        <MaterialTable
            columns={[
                { title: 'ID', field: 'id', type: 'numeric'},
                { title: 'Title', field: 'title'},
                { title: 'Description', field: 'desc', sorting: false},
                { title: 'Scheduled Start', field: 'start', type: 'datetime'},
                { title: 'Scheduled End *', field: 'end', type: 'datetime'},
                { title: 'Duration (s)', field: 'duration'},
            ]}
            data={this.state.table}
            actions={[
            {
                icon: 'cancel',
                tooltip: 'Cancel Test',
                onClick: (event, rowData) => this.cancelJob(rowData)
            }
            ]}
            options={{
                sorting: true,
                search: false,
                showTitle: false,
                actionsColumnIndex: -1
            }}
          />
          <p>* Endtime is an estimate based on time-padding between tests, implemented on the server.</p>
      <EndpointSnackbar ref={this.child} />
      </Container>
    );
  }
}

ScheduledTests.propTypes = {
    classes:    PropTypes.object.isRequired,
    userID:     PropTypes.number.isRequired
};

export default withStyles(useStyles)(ScheduledTests);