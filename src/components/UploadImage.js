/*
    UploadImage.js

    This component is used to render the tab panel to allow a user to upload an image file to the
    application in one of the pre-approved formats. Fields are included to accept a name, description,
    OS and platform for each image, and validation is performed on these fields when the user attempts
    to submit the form, before attempting to send to the back-end. OS and Platform options are loaded
    dynamically by mapping over the relevant lists in constants/index.js

    By making use of the drop-zone's 'acceptedFiles' attribute, we can effectively restrict which file
    types the user is able to upload.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Select from '@material-ui/core/Select';
import CustomButton from './CustomButton';
import {DropzoneArea} from 'material-ui-dropzone';
import EndpointSnackbar from './EndpointSnackbar';
import Endpoint from '../Endpoint';
import * as endpoints from '../constants/endpoints';
import * as constants from '../constants/';
import TextField from "@material-ui/core/TextField";
import MenuItem from '@material-ui/core/MenuItem';

// File Upload Styling.
const useStyles = ({
    root: {
        '& .MuiTextField-root': {
            marginTop: "0.5em",
            marginBottom: "1.2em",
        },
        '& .selectBox': {
            width: "100%",
            borderStyle: "solid",
            borderWidth: "1px",
            borderColor: "lightgray",
            borderRadius: "3px",
            marginBottom: "2em",
            marginTop: "1em",
            height: "3.5em",
        },
    },
    dropZone: {
        minHeight: "200px",
    },
    requiredText: {
        float: "right",
        color: "#666666",
        fontWeight: 500,
    },
    formField: {
        textAlign: "left",
    },
    submitButton: {
        background: "#6BC193",
        height: 48,
        paddingTop: "2em",
        paddingBottom: "2em",
        paddingLeft: "3em",
        paddingRight: "3em",
        marginBottom: "20px",
        lineHeight: 0,
        '&:hover': {
            background: '#84CBA5'
        }
    },
    uploadImageTitle: {
        marginTop: "1em",
        marginBottom: "1em",
    },
    uploadImageField: {
        marginTop: "3em",
        marginBottom: "3em",
    }
});

// Draggable File Upload Component.
class UploadImage extends React.Component {
    constructor(props){
        super(props);
        this.child = React.createRef();
        this.state = {
            name: "",
            desc: "",
            os: "",
            osKey: "",
            platform: "",
            platformKey: "",
            files: [],
            nameError: false,
            descError: false,
            osError: false,
            platformError: false,
            nameErrorMsg: "",
            descErrorMsg: "",
            osErrorMsg: "",
            platformErrorMsg: "",
        };
    }

    // Name text input change event.
    onNameChange(e) {
        this.setState({ name: e.target.value });
    }

    // Description text input change event.
    onDescChange(e) {
        this.setState({ desc: e.target.value });
    }

    // OS text input change event.
    onOSChange(e) {
        this.setState({ os: e._targetInst.stateNode.dataset.value,
            osKey: e._targetInst.stateNode.dataset.osKey,
            osError: false
        });
    }

    // Platform text input change event.
    onPlatformChange(e) {
        this.setState({ platform: e._targetInst.stateNode.dataset.value,
            platformKey: e._targetInst.stateNode.dataset.platformKey,
            platformError: false
        });
    }

    // File input change event.
    handleChange(files) {
        this.setState({ files: files });
    }

    // Alert closed by user or automatically after 5 seconds.
    closeAlert() {
        this.setState({ alertOpen: false });
    }

    // Name validation.
    validateName() {
        const maxLengthName = 50;

        if (this.state.name.length === 0) {
            this.setState({ nameError: true, nameErrorMsg: "Image name is required." });
            return false;
        }
        else if (this.state.name.length > maxLengthName) {
            const msg = "Image name exceeds max length of " + maxLengthName;
            this.setState({ nameError: true, nameErrorMsg: msg });
            return false
        }
        else {
            this.setState({ nameError: false, nameErrorMsg: "" });
            return true
        }
    }

    // Description validation.
    validateDesc() {
        const maxLengthDesc = 200;

        if (this.state.desc.length > maxLengthDesc) {
            const msg = "Image description exceeds max length of " + maxLengthDesc;
            this.setState({ descError: true, descErrorMsg: msg });
            return false;
        }
        else {
            this.setState({ descError: false, descErrorMsg: "" });
            return true;
        }
    }

    // OS validation.
    validateOS() {
        if(this.state.os !== ""){
            this.setState({ osError: false, osErrorMsg: "" });
            return true;
        } else {
            const msg = "No OS has been selected.";
            this.setState({ osError: true, osErrorMsg: msg });
            this.child.current.customMessage("warning", "Failed to submit. No OS has been selected.");
            return false;
        }
    }

    // Platform validation.
    validatePlatform() {
        if(this.state.platform !== ""){
            this.setState({ platformError: false, platformErrorMsg: "" });
            return true;
        } else {
            const msg = "No platform has been selected.";
            this.setState({ platformError: true, platformErrorMsg: msg });
            this.child.current.customMessage("warning", "Failed to submit. No platform has been selected.");
            return false;
        }
    }

    // Image Validation.
    validateImage() {
        if (this.state.files.length === 0) {
            this.child.current.customMessage("warning", "Failed to upload. No image file has been selected.");
            return false;
        }

        return true;
    }

    // Submits the File.
    async submit() {
        let passValidation = true;

        // Validation of text inputs.
        if (!this.validateName()) { passValidation = false; }
        if (!this.validateDesc()) { passValidation = false; }
        if (!this.validateOS()) { passValidation = false; }
        if (!this.validatePlatform()) { passValidation = false; }
        if (!this.validateImage()) { passValidation = false; }

        // Post to endpoint.
        if (passValidation) {
            const data = new FormData();
            const userID = this.props.userID;

            data.append("user_id", userID);
            data.append("image_name", this.state.name);
            data.append("image_description", this.state.desc);
            data.append("image_os", this.state.osKey);
            data.append("image_type", this.state.platformKey);
            data.append("image", this.state.files[0]);

            const uploadImageEndpoint = new Endpoint(
                endpoints.UploadImage,
                this.successCallback.bind(this),
                this.errorCallback.bind(this)
            );
            uploadImageEndpoint.uploadFile(data);
        }
    }

    // Endpoint success callback.
    successCallback(response) {
        this.child.current.customMessage("success", response.data.message);
        // Key incremented to force re-render of the dropzone.
        // Clears the selected files.
        this.setState({ files: [], key: this.state.key + 1 });
        this.props.switchTab(constants.upload_page_tab_index["Manage-Images"]);
    }

    // Endpoint error callback.
    errorCallback(error, msg) {
        this.child.current.customMessage("error", msg);
        // Clears the erroneous image file.
        this.setState({files: [], key: this.state.key + 1});
    }

    renderFileTypes() {
        let fileTypesString = "";
        constants.accepted_image_file_types.map(type => fileTypesString += type + " ");
        return fileTypesString;
    }

    renderOSOptions() {
        return constants.image_os.map((os) => {
            return (
                <MenuItem
                    data-os-key={os.key}
                    value={os.value}
                    key={os.key}
                    name={os.key}>
                    {os.value}
                </MenuItem>
            );
        });
    }

    renderPlatformOptions() {
        return constants.image_platform.map((platform) => {
            return (
                <MenuItem
                    data-platform-key={platform.key}
                    value={platform.value}
                    key={platform.key}
                    name={platform.key}>
                    {platform.value}
                </MenuItem>
            );
        });
    }

    render() {
        const { classes } = this.props;

        return (
            <Container className={classes.root}>
                <Typography className={classes.uploadImageTitle} variant="h4" component="h1">Upload Image</Typography>
                <Typography className={classes.requiredText} variant="h6" component="h5">*: Required</Typography>
                <Typography className={classes.formField} variant="h6" component="h3">Image Name *</Typography>
                <TextField
                    className={classes.uploadImageField}
                    id="upload-image-name"
                    variant="outlined"
                    fullWidth={true}
                    required
                    onChange={this.onNameChange.bind(this)}
                    error={this.state.nameError}
                    helperText={this.state.nameErrorMsg}
                    inputProps={{ style: { left: 15, top: 5, position: "relative" } }}
                />
                <Typography className={classes.formField} variant="h6" component="h3">Image Description</Typography>
                <TextField
                    className={classes.uploadImageField}
                    id="upload-image-desc"
                    label="Image Description"
                    variant="outlined"
                    fullWidth={true}
                    multiline
                    rows="5"
                    rowsMax="5"
                    onChange={this.onDescChange.bind(this)}
                    error={this.state.descError}
                    helperText={this.state.descErrorMsg}
                />
                <Typography className={classes.formField} variant="h6" component="h3">Image Platform *</Typography>
                <Select className="selectBox"
                        native={false}
                        renderValue={() => {if (this.state.platform==="") {return "None Selected"} else {return this.state.platform}}}
                        displayEmpty={true}
                        error={this.state.platformError}
                        name={this.state.platformKey}
                        value={this.state.platform}
                        onChange={this.onPlatformChange.bind(this)}>
                    {this.renderPlatformOptions()}
                </Select>
                <Typography className={classes.formField} variant="h6" component="h3">Image OS *</Typography>
                <Select className="selectBox"
                        native={false}
                        renderValue={() => {if (this.state.os==="") {return "None Selected"} else {return this.state.os}}}
                        displayEmpty={true}
                        error={this.state.osError}
                        name={this.state.osKey}
                        value={this.state.os}
                        onChange={this.onOSChange.bind(this)}>
                    {this.renderOSOptions()}
                </Select>
                <DropzoneArea
                    key={this.state.key}
                    drop-zone-class={classes.dropZone}
                    onChange={this.handleChange.bind(this)}
                    dropzoneText={'Click or drag and drop to upload an image file. Accepted file types are: ' + this.renderFileTypes()}
                    acceptedFiles={constants.accepted_image_file_types}
                    filesLimit={1}
                    showFileNames={true}
                    maxFileSize={1000000}
                    height={200}
                    showPreviewsInDropzone={true}
                    useChipsForPreview={true}
                />
                <br />
                <EndpointSnackbar ref={this.child} />
                <CustomButton className={classes.submitButton} onClick={this.submit.bind(this)}>Upload</CustomButton>
            </Container>
        );
    }
}

// Higher-Order component.
UploadImage.propTypes = {
    classes:      PropTypes.object.isRequired,
    userID:       PropTypes.number.isRequired,
    switchTab:    PropTypes.func.isRequired,
};

export default withStyles(useStyles)(UploadImage);
