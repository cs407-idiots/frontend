/*
    SignOut.js

    This component facilitates a user signing out from Lucidlab. When the user clicks the SignOut
    button in the Navbar, it both redirects them back to the homepage and also deletes their UserID
    and SignedIn cookies.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CustomButton from './CustomButton';
import {Link} from 'react-router-dom';

const useStyles = theme => ({
    root: {},
    signOutBtn: {
        background: "linear-gradient(45deg, #08C27C 30%, #06CC86 90%)",
        borderRadius: "20px",
        lineHeight: 0,
    }
});

class SignOut extends React.Component {
    render() {
        const { classes, action } = this.props;

        if (this.props.signedIn) {
            return (
                <div>
                    <CustomButton component={Link} to="/" className={classes.signOutBtn} onClick={action}>
                        Sign Out
                    </CustomButton>
                </div>
            );
        }
        else {
            return null;
        }
    }
}

SignOut.propTypes = {
    classes:  PropTypes.object.isRequired,
    signedIn: PropTypes.bool.isRequired,
    action:   PropTypes.func.isRequired,
};

export default withStyles(useStyles)(SignOut);