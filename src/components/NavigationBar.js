/*
    NavigationBar.js

    This component is used as the fixed navigation bar across all of LucidLab's pages.
    It allows the user to access any page on the site from any other page, as well as facilitating
    signing in/up/out by launching the appropriate modal.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import AssignmentIcon from '@material-ui/icons/Assignment';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import Tooltip from '@material-ui/core/Tooltip';
import {Link} from 'react-router-dom';
import SignUp from './SignUp';
import SignIn from './SignIn';
import SignOut from './SignOut';
import {withRouter} from 'react-router-dom';

const useStyles = ({
    title: {
        flexGrow: 1,
        textAlign: "left",
        fontSize: "1.5rem",
        outline: "0px",
    },
    nav: {
        backgroundColor: "#4BA173",
    },
    link: {
        color: "#FFFFFF",
        outline: "0px",
    },
    iconLink: {
        "&:focus": {
            backgroundColor: "#5BB183",
            outline: "0px",
        },
    },
});

class NavBar extends React.Component {
    signIn(userID) {
        this.props.signedInChange(true, userID);
    }

    signOut() {
        this.props.signedInChange(false, undefined);
    }

    render() {
        const { classes, title } = this.props;
        const signIn = this.signIn.bind(this);
        const signOut = this.signOut.bind(this);

        return (
            <AppBar className={classes.nav} position="static">
                <Toolbar>

                    <Typography variant="h6" className={classes.title}>
                        <Link className={classes.link} variant="inherit" to="/">{title}</Link>
                    </Typography>

                    <Tooltip title="New Test Config">
                        <Link className={classes.link} variant="inherit" to="/tests">
                            <IconButton className={classes.iconLink} color="inherit">
                                <NoteAddIcon />
                            </IconButton>
                        </Link>
                    </Tooltip>

                    <Tooltip title="Manage Uploads">
                        <Link className={classes.link} variant="inherit" to="/uploads">
                            <IconButton className={classes.iconLink} color="inherit">
                                <BusinessCenterIcon />
                            </IconButton>
                        </Link>
                    </Tooltip>

                    <Tooltip title="Test Results">
                        <Link className={classes.link} variant="inherit" to="/results">
                            <IconButton className={classes.iconLink} color="inherit">
                                <AssignmentIcon />
                            </IconButton>
                        </Link>
                    </Tooltip>

                    <Tooltip title="LucidLab Status">
                        <Link className={classes.link} variant="inherit" to="/metrics">
                            <IconButton className={classes.iconLink} color="inherit">
                                <TrendingUpIcon />
                            </IconButton>
                        </Link>
                    </Tooltip>

                    <Tooltip title="Sign Out">
                        <SignOut signedIn={this.props.signedIn} action={signOut} />
                    </Tooltip>
                    <Tooltip title="Sign Up">
                        <SignUp signedIn={this.props.signedIn} action={signIn} />
                    </Tooltip>
                    <Tooltip title="Sign In">
                        <SignIn signedIn={this.props.signedIn} action={signIn} />
                    </Tooltip>

                </Toolbar>
            </AppBar>
        );
    }
}

NavBar.propTypes = {
    classes: PropTypes.object.isRequired,
    title:   PropTypes.string.isRequired,
};

export default withRouter(withStyles(useStyles)(NavBar));