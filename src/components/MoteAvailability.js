/*
    MoteAvailability.js

    This file serves the mote availability metrics page with the Status Page Tab Panel
    Offers three distinct metrics for the user to assess:
        - A Table listing currently connected motes and their platform and status
        - A Graph of the currently connected motes, showing connections visually and with RSSI scores
            labelled on edges
        - Two Bar Chart graphs showing the CCA score of a user selected mote, and the CCA of the whole
            system.
        - Two Bar Chart graphs showing the Noise Floor data of a user selected mote, and the Noise Floor data
            of the whole system.
 */

import React from 'react';
import '../App.css';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import "../network.css";
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Chip from '@material-ui/core/Chip';
import CachedIcon from '@material-ui/icons/Cached';
import MaterialTable from "material-table";
import Endpoint from "../Endpoint";
import * as endpoints from "../constants/endpoints";
import * as constants from "../constants";
import Graph from "react-graph-vis";
import Chart from "react-google-charts";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

// Current date.
function getCurrentDate(date) {
    return ((date.getDate() < 10) ? "0" : "") + date.getDate() + "/" +(((date.getMonth() + 1) < 10) ? "0" : "") + (date.getMonth() + 1) + "/" + date.getFullYear();
}

// Current time.
function getCurrentTime(date) {
    return ((date.getHours() < 10) ? "0" : "") + date.getHours() + ":" + ((date.getMinutes() < 10) ? "0" : "") + date.getMinutes() + ":" + ((date.getSeconds() < 10) ? "0" : "") + date.getSeconds();
}

// Vis.js graph options.
const graphOptions = {
    autoResize: true,
    layout: {
        hierarchical: false
    },
    edges: {
        color: "#A0A0A0",
        smooth: true,
        font: {
            size: 12,
        },
    },
    nodes: {
        borderWidth: 1,
        borderWidthSelected: 2,
        font: "18px Roboto black",
        shape: "box",
    },
    interaction: {
        dragNodes: true,
        dragView: true,
        zoomView: true,
        navigationButtons: true,
        hover: true,
        selectable: false,
        selectConnectedEdges: false,
        tooltipDelay: 20
    }
};

const useStyles = ({
    root: {},
    title: {
        marginTop: "1em",
        marginBottom: "1em",
    },
    moteAvailability: {
        marginTop: "4em",
        textAlign: "left",
    },
    lastUpdated: {
        textAlign: "right",
        fontSize: "0.7125rem",
        paddingTop: "1px",
        float: "right",
        marginTop: "1.5em",
    },
    diagram: {
        border: "1px solid #E0E0E0",
        height: "400px",
        marginTop: "1em",
    },
    chartDiagram: {
        border: "1px solid #E0E0E0",
        height: "434px",
        marginTop: "1em",
        background: "white"
    },
    moteSelection: {
        height: "32px",
        position: "relative",
        float: "left",
        width: "100%",
        zIndex: "999",
        "& p": {
            float: "left",
            marginTop: "3%",
            marginLeft: "18%"
        }
    },
    selectBox: {
        float: "left",
        marginLeft: "1%",
        marginTop: "2.6%"
    },
    chart: {
        display: "inline-block",
        float: "left",
        height: "400px",
        width: "50%",
    },
    chartTitle: {
        marginTop: "4em",
        textAlign: "left",
        marginBottom: "1em"
    },
    moteTableTitle: {
        marginTop: "2em",
        textAlign: "left",
        marginBottom: "1em"
    },
    noDataBody: {
        background: "lightgrey",
        height: "100px",
        marginTop: "150px",
        width: "90%",
        marginLeft: "5%"
    },
    noDataText: {
        paddingTop: "30px"
    },
    visNetwork: {
        overflow: "visible"
    }
});


class MoteAvailability extends React.Component {
    RSSIs;

    constructor(props) {
        super(props);
        this.state = {
            moteIDList: [],
            table: [],
            graph: { nodes: [], edges: [] },
            lastMapSync: "Last Updated: Never",
            lastTableSync: "Last Updated: Never",
            lastCCASync: "Last Updated: Never",
            lastNoiseFloorSync: "Last Updated: Never",
            CCAData: {
                mote: [],
                global: []
            },
            CCAMote: "",
            NoiseFloorData: {
                mote: [],
                global: []
            },
            NoiseFloorMote: ""
        };
    }

    componentDidMount() {
        this.fetchMotes();
        this.fetchMap();
        this.fetchCCACharts();
        this.fetchNoiseFloorCharts();
    }

    // Fetches the map of currently connected motes
    fetchMap = async () => {
        const getMapEndpoint = new Endpoint(
            endpoints.GetRSSIValues,
            this.buildGraph.bind(this),
            this.mapError.bind(this)
        );

        await getMapEndpoint.asyncGet();
    };

    // Builds the graph of currently connected motes, and displays on the page
    buildGraph(response) {
        const graph = response.data.map;
        const nodes = [];
        const edges = [];

        // Constructs the set of nodes.
        for (let sourceNode in graph) {
            // For node colouring:
            let nodeStatus = graph[sourceNode].status;
            let nodeColour = "#D2E5FF";
            if (nodeStatus  === "Idle") {
                nodeColour = "#4BA173";
            }
            else if (nodeStatus === "Running test") {
                nodeColour = "#fcba03";
            }
            else if (nodeStatus  === "Connection timeout") {
                nodeColour = "#f3513f";
            }
            else {
                console.log("Unknown Status");
            }

            nodes.push({ id: parseInt(sourceNode), label: sourceNode.toString(), color: { background: nodeColour, border: nodeColour }, title: "Status: " + nodeStatus});

            const neighbours = graph[sourceNode].RSSIs;
            for (let key in neighbours) {
                edges.push({ from: parseInt(sourceNode), to: parseInt(key), label: neighbours[key].toString() });
            }
        }

        const newDate = new Date();
        const datetime = "Last Updated: " + getCurrentDate(newDate) + " @ " + getCurrentTime(newDate);

        this.setState({ graph: { nodes: nodes, edges: edges }, lastMapSync: datetime });
    }

    // Error callback method for retrieving maps related to the mote availability graph
    mapError(error, message) {
        console.log("Mote Retrieval Error");
        console.log(error);
        console.log(message);
    }

    // Fetches CCA data for use in the mote and global bar charts on the page
    fetchCCACharts = async () => {
        // Check for any new motes when updating chart
        this.fetchMotes();

        const data = {"mote_id": this.state.CCAMote};

        const getCCAEndpoint = new Endpoint(
            endpoints.GetCCAValues,
            this.fetchCCASuccess.bind(this),
            this.moteError.bind(this)
        );

        await getCCAEndpoint.asyncGet(data);

        const newDate = new Date();
        const datetime = "Last Updated: " + getCurrentDate(newDate) + " @ " + getCurrentTime(newDate);

        this.setState({ lastCCASync: datetime });
    };

    // Fetches noise floor data for use in the mote and global bar charts on the page
    fetchNoiseFloorCharts = async () => {
        // Check for any new motes when updating chart
        this.fetchMotes();

        const data = {"mote_id": this.state.NoiseFloorMote};

        const getNoiseFloorEndpoint = new Endpoint(
            endpoints.GetNoiseFloorValues,
            this.fetchNoiseFloorSuccess.bind(this),
            this.moteError.bind(this)
        );

        await getNoiseFloorEndpoint.asyncGet(data);

        const newDate = new Date();
        const datetime = "Last Updated: " + getCurrentDate(newDate) + " @ " + getCurrentTime(newDate);

        this.setState({ lastNoiseFloorSync: datetime });
    };

    fetchCCASuccess = async (response) => {
        this.setState({
            CCAData: response.data
        })
    };

    fetchNoiseFloorSuccess = async (response) => {
        this.setState({
            NoiseFloorData: response.data
        })
    };


    // Builds the cca chart on the page dynamically, based on the requested chart type
    buildCCAChart = (type, mote) => {
        const chartOptions = {
            title: "",
            hAxis: {title: "Channel", viewWindow: {min: 0, max: 16}},
            vAxis: {title: "CCA", viewWindow: {min: 0, max: 1000}},
            legend: "none"
        };

        const barColour = constants.cca_bar_chart_colour;
        const dataFrame = [
            ["Channel", "CCA Value", {role: "style"}]
        ];

        let dataSource = this.state.CCAData;

        // Switches between types of graph requested
        switch (type) {
            case "mote":
                chartOptions.title = "Mote " + mote + " CCA";
                dataSource = dataSource.mote;
                break;
            case "global":
                chartOptions.title = "Global Testbed CCA";
                dataSource = dataSource.global;
                break;
            default:
                console.log("Chart Type Switching Error.");
        }

        // Checks that the dataSource contains data
        if (dataSource !== undefined && dataSource.length > 0) {
            // Maps over the datasource, creating a data record for each data point found
            const ccaData = dataSource.map(
                (datum, index) => {
                    return [(index+1).toString(),
                        datum,
                        barColour];
                });

            const data = dataFrame.concat(ccaData);

            // Returns the chart with custom options and relevant data
            return (<Chart
                chartType="ColumnChart"
                height="400px"
                options={chartOptions}
                data={data}
            />);
        } else {
            // If no data found in the datasource, display an error splash
            return (
                <div className={this.props.classes.noDataBody} >
                    <p className={this.props.classes.noDataText}>
                        CCA data not currently available.
                    </p>
                </div>
            );
        }
    };

    // Builds the noise floor chart on the page dynamically, based on the requested chart type
    buildNoiseFloorChart = (type, mote) => {
        const chartOptions = {
            title: "",
            hAxis: {title: "Channel", viewWindow: {min: 0, max: 16}},
            vAxis: {title: "Noise Floor (dBm)", viewWindow: {min: 0, max: -150}},
            legend: "none"
        };

        const barColour = constants.cca_bar_chart_colour;
        const dataFrame = [
            ["Channel", "Noise Floor (dBm)", {role: "style"}]
        ];

        let dataSource = this.state.NoiseFloorData;

        // Switches between types of graph requested
        switch (type) {
            case "mote":
                chartOptions.title = "Mote " + mote + " Noise Floor";
                dataSource = dataSource.mote;
                break;
            case "global":
                chartOptions.title = "Global Testbed Noise Floor";
                dataSource = dataSource.global;
                break;
            default:
                console.log("Chart Type Switching Error.");
        }

        // Checks that the dataSource contains data
        if (dataSource !== undefined && dataSource.length > 0) {
            // Maps over the datasource, creating a data record for each data point found
            const noiseFloorData = dataSource.map(
                (datum, index) => {
                    return [(index+1).toString(),
                        datum,
                        barColour];
                });

            const data = dataFrame.concat(noiseFloorData);

            // Returns the chart with custom options and relevant data
            return (<Chart
                chartType="ColumnChart"
                height="400px"
                options={chartOptions}
                data={data}
            />);
        } else {
            // If no data found in the datasource, display an error splash
            return (
                <div className={this.props.classes.noDataBody} >
                    <p className={this.props.classes.noDataText}>
                        Noise Floor data not currently available.
                    </p>
                </div>
            );
        }
    };

    // Fetches the jobs from the endpoint.
    fetchMotes = async () => {
        const getMotesEndpoint = new Endpoint(
            endpoints.GetMotes,
            this.moteSuccess.bind(this),
            this.moteError.bind(this)
        );

        await getMotesEndpoint.asyncGet();
    };

    moteSuccess(response) {
        const motes = response.data.status;
        let moteList = [];
        let moteIDs = [];
        let moteEntry = {};

        for (let key in motes) {
            moteEntry = {id: key,
                platform: motes[key].platform,
                status: motes[key].status,
            };
            moteIDs.push(key);
            moteList.push(moteEntry);
        }

        const newDate = new Date();
        const datetime = "Last Updated: " + getCurrentDate(newDate) + " @ " + getCurrentTime(newDate);

        this.setState({
            moteIDList: moteIDs,
            table: moteList,
            lastTableSync: datetime
        });

        // If motes are available initialise CCA & Noise Floor map to be the first mote
        if (this.state.CCAMote === "" && this.state.moteIDList.length > 0){
            this.setState({
                CCAMote: this.state.moteIDList[0],
                NoiseFloorMote: this.state.moteIDList[0]
            }, () => {
                this.fetchCCACharts();
                this.fetchNoiseFloorCharts();
            });
        }
    }

    moteError(error, message) {
        console.log("Mote Retrieval Error");
        console.log(error);
        console.log(message);
    }

    // Renders the list of motes.
    renderMotes() {
        if (this.state.moteIDList !== []) {
            return this.state.moteIDList.map((mote) => {
                return (
                    <MenuItem
                        data-controller-key={mote}
                        value={mote}
                        key={mote}
                        name={"mote:" + mote}>
                        {mote}
                    </MenuItem>
                );
            });
        }
        else {
            return null;
        }
    }

    onNoiseFloorMoteChange = (e) => {
        this.setState({
            NoiseFloorMote: e.target.value
        }, () => {
            this.fetchNoiseFloorCharts();
        });
    };

    onCCAMoteChange = (e) => {
        this.setState({
            CCAMote: e.target.value
        }, () => {
            this.fetchCCACharts();
        });
    };

    render() {
    
        const { classes } = this.props;

        return (
            <Container className={classes.root}>
                <Typography className={classes.moteTableTitle} variant="h6" component="h1">Mote Availability Table</Typography>
                <MaterialTable
                    columns={[
                        { title: 'Mote ID', field: 'id', type: 'numeric'},
                        { title: 'Platform', field: 'platform'},
                        { title: 'Status', field: 'status'},
                    ]}
                    data={this.state.table}
                    options={{
                        sorting: true,
                        search: false,
                        showTitle: false,
                        actionsColumnIndex: -1
                    }}
                />
                <Chip className={classes.lastUpdated} onClick={this.fetchMotes} variant="outlined" color="secondary" label={this.state.lastTableSync} icon={<CachedIcon />}/>

                <Typography className={classes.moteAvailability} variant="h6" component="h1">Mote RSSI Map</Typography>
                <div className={classes.diagram}>
                    <Graph
                        graph={this.state.graph}
                        options={graphOptions}
                        style={{ height: "400px" }}
                    />
                </div>
                <Chip className={classes.lastUpdated} onClick={this.fetchMap} variant="outlined" color="secondary" label={this.state.lastMapSync} icon={<CachedIcon />}/>

                <Typography className={classes.chartTitle} variant="h6" component="h1">Mote CCA Charts</Typography>
                <div className={classes.chartDiagram}>
                    <div className = {classes.moteSelection}>
                        <p>Select Mote ID: </p>
                        <Select
                            className={classes.selectBox}
                            native={false}
                            renderValue={() => {return this.state.CCAMote}}
                            displayEmpty={true}
                            name={this.state.CCAMote}
                            value={this.state.CCAMote}
                            onChange={this.onCCAMoteChange.bind(this)}>
                            {this.renderMotes()}
                        </Select>
                    </div>
                    <div className={classes.charts}>
                        <div className={classes.chart}>
                            {this.buildCCAChart("mote", this.state.CCAMote)}
                        </div>

                        <div className={classes.chart}>
                            {this.buildCCAChart("global")}
                        </div>
                    </div>
                </div>
                <Chip className={classes.lastUpdated} onClick={this.fetchCCACharts} variant="outlined" color="secondary" label={this.state.lastCCASync} icon={<CachedIcon />}/>

                <Typography className={classes.chartTitle} variant="h6" component="h1">Mote Noise Floor Charts</Typography>
                <div className={classes.chartDiagram}>
                    <div className = {classes.moteSelection}>
                        <p>Select Mote ID: </p>
                        <Select
                            className={classes.selectBox}
                            native={false}
                            renderValue={() => {return this.state.NoiseFloorMote}}
                            displayEmpty={true}
                            name={this.state.NoiseFloorMote}
                            value={this.state.NoiseFloorMote}
                            onChange={this.onNoiseFloorMoteChange.bind(this)}>
                            {this.renderMotes()}
                        </Select>
                    </div>
                    <div className={classes.charts}>
                        <div className={classes.chart}>
                            {this.buildNoiseFloorChart("mote", this.state.NoiseFloorMote)}
                        </div>

                        <div className={classes.chart}>
                            {this.buildNoiseFloorChart("global")}
                        </div>
                    </div>
                </div>
                <Chip className={classes.lastUpdated} onClick={this.fetchNoiseFloorCharts} variant="outlined" color="secondary" label={this.state.lastNoiseFloorSync} icon={<CachedIcon />}/>
            </Container>
        );
    }
}

MoteAvailability.propTypes = {
    classes: PropTypes.object.isRequired,
    userID: PropTypes.number.isRequired
};

export default withStyles(useStyles)(MoteAvailability);