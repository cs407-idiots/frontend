/*
    CreateTest.js

    This file hosts the online test configurator, which provides users with a form with which they
    can create a test configuration from within LucidLab, rather than having to provide a new YAML
    each time.

    It has necessary validation implemented including type checking, length checks and required field
    enforcement. It allows the user to dynamically add and configure as many Images as they wish, and
    loads the currently available motes into each Image Configuration box.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import CustomButton from './CustomButton';
import EndpointSnackBar from './EndpointSnackbar';
import Endpoint from '../Endpoint';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ClearIcon from '@material-ui/icons/Clear';
import Button from '@material-ui/core/Button';
import {blobToSHA1} from 'file-to-sha1';
import * as endpoints from '../constants/endpoints';
import * as constants from '../constants'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {Redirect} from 'react-router-dom';

const yaml = require("js-yaml");
const maxLengthName = 50;
const maxLengthDesc = 200;

const useStyles = theme => ({
    root: {
        '& .MuiTextField-root': {
            marginTop: "2em",
            marginBottom: "20px",
        },
        '& .selectBox': {
            width: "100%",
            borderStyle: "solid",
            borderWidth: "1px",
            borderColor: "lightgray",
            borderRadius: "3px",
            marginBottom: "2em",
            marginTop: "1em",
            height: "3.5em",
        },
    },
    createTestTitle: {
        marginTop: "1em",
        marginBottom: "1em",
    },
    createTestField: {
        marginTop: "1em!important",
        marginBottom: "2em!important",
    },
    createTestButton: {
        background: "#6BC193",
        height: 48,
        paddingTop: "2em",
        paddingBottom: "2em",
        paddingLeft: "3em",
        paddingRight: "3em",
        '&:hover': {
            background: '#84CBA5'
        },
        lineHeight: 0,
        marginBottom: "2em",
    },
    addImageButton: {
        background: '#fcba03',
        height: 48,
        paddingTop: "2em",
        paddingBottom: "2em",
        paddingLeft: "3em",
        paddingRight: "3em",
        lineHeight: 0,
        marginBottom: "2em",
        '&:hover': {
            background: 'rgba(252,186,3,0.76)'
        },
        '&:focus': {
            background: "#fcba03"
        },
    },
    formField: {
        textAlign: "left",
    },
    checkbox: {
        float: "left",
    },
    imageField: {
        marginTop: "1em!important",
        marginBottom: "2em!important",
        float: "left",
    },
    imageSelectionArea: {
        padding: "1em",
        border: "1px solid #78C27C",
        borderRadius: "5px",
        marginBottom: "2em",
        position: "relative",
        marginTop: "1em",
    },
    removeImageButton: {
        position: "absolute",
        right: "0.25em",
        top: "0.25em",
        lineHeight: 0,
    },
    requiredText: {
        float: "right",
        color: "#666666",
        fontWeight: 500,
    },
    smallButton: {
        border: 0,
        borderRadius: 3,
        color: "white",
        height: 24,
        paddingTop: "1em",
        paddingBottom: "1em",
        paddingLeft: "1em",
        paddingRight: "1em",
        fontWeight: "bold",
        marginLeft: "0.25em",
        marginRight: "0.25em",
        fontSize: "0.75rem",
        float: "right",
        lineHeight: 0,
    },
    selectButton: {
        background: "#f3513f",
        border: "1px solid #D1371F",
        lineHeight: 0,
        '&:hover': {
            background: '#f67d6f'
        },
        '&:focus': {
            background: "#f3513f"
        },
    },
    deselectButton: {
        background: "#f3513f",
        border: "1px solid #F5977D",
        lineHeight: 0,
        '&:hover': {
            background: '#f67d6f'
        },
        '&:focus': {
            background: "#f3513f"
        },
    },
});

// Create Test Tab View.
class CreateTestTab extends React.Component {
    constructor(props) {
        super(props);
        this.child = React.createRef();
        this.state = {
            name: "",
            desc: "",
            duration: "",
            nameError: false,
            descError: false,
            durationError: false,
            nameErrorMsg: "",
            descErrorMsg: "",
            durationErrorMsg: "",
            controller: "",
            controllerID: undefined,
            controllerError: false,
            controllerErrorMsg: false,
            controllersLoaded: false,
            moteList: [],
            moteIDList: [],
            imageIDError: false,
            images: [{
                id: "",
                motes: [false],
                error: false,
                msg: "",
            }],
            redirect: false,
        };
    }

    // Gets the list of IoT controllers.
    componentDidMount() {
        // Form data construction.
        const data = { "user_id": this.props.userID };
        // Gets the user's IoT controllers from the endpoint.
        const getControllersEndpoint = new Endpoint(endpoints.GetControllers, this.getControllersSuccessCallback.bind(this), this.getControllersErrorCallback.bind(this));
        getControllersEndpoint.get(data);
        this.getMotes();
    }

    getControllersSuccessCallback(response) {
        this.controllerList = response.data.iot_controllers;
        this.setState({ controllersLoaded: true });
    }

    getControllersErrorCallback(error, message) {
        console.log("IoT Controller Retrieval Error");
        console.log(error);
        console.log(message);
        this.controllerList = [];
        this.setState({ controllersLoaded: true });
    }

    // Sets the state to trigger redirection.
    setRedirect = () => {
        this.setState({
            redirect: true
        })
    };

    // Redirects to the metric page upon successful form submission.
    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/metrics' />
        }
    };

    getMotes() {
        const getMotesEndpoint = new Endpoint(
            endpoints.GetMotes,
            this.moteSuccess.bind(this),
            this.moteError.bind(this)
        );

        getMotesEndpoint.get();
    }

    moteSuccess(response) {
        let moteIDList = [];
        for (let key in response.data.status) {
            moteIDList.push(key);
        }

        // Set all motes to unselected at first
        const emptyMoteSelection = [];
        for (let i = 0; i < moteIDList.length; i++) {
            emptyMoteSelection.push(false);
        }

        this.setState({
            moteList: response.data.status,
            moteIDList: moteIDList,
            images: [{
                id: "",
                motes: emptyMoteSelection,
                error: false,
                msg: "",
            }],
        });
    }

    moteError(error, message) {
        console.log("Mote Retrieval Error");
        console.log(error);
        console.log(message);
    }

    // Name text input change event.
    onNameChange(e) {
        this.setState({ name: e.target.value });
    }

    // Description text input change event.
    onDescChange(e) {
        this.setState({ desc: e.target.value });
    }

    // Test duration input change event.
    onDurationChange(e) {
        this.setState({ duration: e.target.value });
    }

    // Image id input change event.
    onImageIdChange(index, e) {
        e.persist();
        this.setState(({ images }) => ({ images:
                images.map((value, i) => {
                    return (i === index) ? Object.assign({}, value, {id: e.target.value}) : value;
                })
        }));
    }

    // Selects/Deselects motes for a given image ID.
    onMoteChange(index, moteIndex, e) {
        e.persist();
        this.setState(({ images }) => ({ images:
                images.map((image, i) => {
                    let newMoteList = image.motes.map((moteValue, j) => {
                        return (i === index && j === moteIndex) ? e.target.checked : moteValue;
                    });
                    return Object.assign({}, image, {motes: newMoteList});
                })
        }));
    }

    // Controller change event.
    onControllerChange(e) {
        this.setState({
            controller: e._targetInst.stateNode.dataset.value,
            controllerID: e._targetInst.stateNode.dataset.controllerKey
        });
    }

    // Removes the controller selection.
    removeController() {
        this.setState({
            controller: "",
            controllerID: undefined
        });
    }

    // Alert closed by user or automatically after 5 seconds.
    closeAlert() {
        this.setState({ alertOpen: false });
    }

    // Test name validation.
    validateName() {
        if (this.state.name.length === 0) {
            this.setState({ nameError: true, nameErrorMsg: "Test name is required." });
            return false;
        }
        else if (this.state.name.length > maxLengthName) {
            const msg = "Test name exceeds max length of " + maxLengthName;
            this.setState({ nameError: true, nameErrorMsg: msg });
            return false
        }
        else {
            this.setState({ nameError: false, nameErrorMsg: "" });
            return true
        }
    }

    // Description validation.
    validateDesc() {
        if (this.state.desc.length > maxLengthDesc) {
            const msg = "Test description exceeds max length of " + maxLengthDesc;
            this.setState({ descError: true, descErrorMsg: msg });
            return false;
        }
        else {
            this.setState({ descError: false, descErrorMsg: "" });
            return true;
        }
    }

    // Duration validation.
    validateDuration() {
        const positiveInt = /^(0|[1-9]\d*)$/;
        if (this.state.duration.length === 0) {
            this.setState({ durationError: true, durationErrorMsg: "Test duration is required." });
            return false;
        }
        else if (!positiveInt.test(this.state.duration)) {
            this.setState({ durationError: true, durationErrorMsg: "Test duration is invalid. Expected integer value."})
            return false;
        }
        else {
            this.setState({ durationError: false, durationErrorMsg: "" });
            return true;
        }
    }

    // Image ID validation.
    validateImageID() {
        const positiveInt = /^(0|[1-9]\d*)$/;
        let valid = true;

        this.setState(({ images }) => ({ images:
                images.map(image => {
                    // Empty Image ID.
                    if (image.id.length === 0) {
                        valid = false;
                        this.setState({ imageIDError: true });
                        return Object.assign({}, image, { error: true, msg: "Image ID is required." });
                    }
                    else if (!positiveInt.test(image.id)) {
                        valid = false;
                        this.setState({ imageIDError: true });
                        return Object.assign({}, image, { error: true, msg: "Image ID is invalid. Expected positive integer value." });
                    }
                    else {
                        this.setState({ imageIDError: false });
                        return Object.assign({}, image, { error: false, msg: "" });
                    }
                })
        }));

        return valid;
    }

    // Adds a blank image to the form.
    addImage() {
        // All motes are selected as false by default.
        const moteSelection = [];
        for (let i = 0; i < this.state.moteIDList.length; i++) {
            moteSelection.push(false);
        }

        const newImage = [{
            id: "",
            motes: moteSelection,
            error: false,
            msg: "",
        }];

        this.setState({
            images: this.state.images.concat(newImage)
        });
    }

    //  Removes an image from the form.
    removeImage(index) {
        let images = this.state.images;

        // Prevent 0 images.
        if (this.state.images.length === 1) {
            this.child.current.customMessage("warning", "Test configuration must contain at least one image.");
            return;
        }

        images.splice(index, 1);

        this.setState({ images: images });
    }

    // Returns True values for all motes that have not timed out
    selectActiveMotes() {
        return this.state.moteIDList.map((value, moteIndex) => {
            return this.state.moteList[value].status !== constants.mote_timeout_status;
        })
    };

    // Selects all motes for an image.
    selectAllMotes(index) {
        this.setState(({ images }) => ({ images:
                images.map((image, i) => {
                    return (i === index) ? Object.assign({}, image, { motes: this.selectActiveMotes() }) : image;
                })
        }));
    }

    // Deselects all motes for an image.
    deselectAllMotes(index) {
        this.setState(({ images }) => ({ images:
                images.map((image, i) => {
                    return (i === index) ? Object.assign({}, image, { motes: image.motes.map(_ => false) }) : image;
                })
        }));
    }

    // Submit button click event.
    createTestSubmit = async () => {
        let passValidation = true;

        // Validation of text inputs.
        await this.validateImageID();

        if (!this.validateName()) { passValidation = false; }
        if (!this.validateDesc()) { passValidation = false; }
        if (!this.validateDuration()) { passValidation = false; }
        if (this.state.imageIDError) { passValidation = false; }

        // Checks the existence of at least one image.
        if (this.state.images.length === 0) {
            passValidation = false;
            this.child.current.customMessage("error", "Test configuration requires at least one image.");
        }

        // Checks for duplicate image ids.
        const imageCount = this.state.images.length;
        const imageSet = new Set(this.state.images.map(value => value.id));
        if (imageCount !== imageSet.size) {
            passValidation = false;
            this.child.current.customMessage("error", "Test configuration contains duplicate image IDs.");
        }

        // Passes validation.
        if (passValidation) {
            this.uploadTest();
        }
    };

    // Uploads a test config.
    async uploadTest() {
        // Mode selection for each image.
        const devices = [];
        for (let i = 0; i < this.state.images.length; i++) {
            let moteIds = [];
            for (let j = 0; j < this.state.moteIDList.length; j++) {
                if (this.state.images[i].motes[j] === true) {
                    moteIds.push(parseInt(this.state.moteIDList[j]));
                }
            }
            devices.push({
                "image-id": parseInt(this.state.images[i].id),
                "mote-ids": moteIds,
            });
        }

        const config = {
            "duration": parseInt(this.state.duration),
            "test-name": this.state.name,
            "test-description": this.state.desc,
            "image-ids": this.state.images.map(image => parseInt(image.id)),
            "image-devices": devices,
        };

        if (this.state.controllerID !== undefined) {
            config["iot-controller-id"] = parseInt(this.state.controllerID);
        }

        // Creates a YAML blob and calculates the checksum value.
        const yamlBlob = new Blob([yaml.safeDump(config)], { type: 'text/yaml' });
        const checksum = await blobToSHA1(yamlBlob);

        // Constructs the form data.
        const data = new FormData();
        const fileName = this.getFileName();
        data.append("user_id", this.props.userID);
        data.append("yaml", yamlBlob, fileName);
        data.append("checksum", checksum);

        // Uploads the configuration to the endpoint.
        const uploadTestEndpoint = new Endpoint(endpoints.UploadConfig, this.successCallback.bind(this), this.errorCallback.bind(this));
        uploadTestEndpoint.uploadFile(data);
    }

    // Gets the filename for the YAML file.
    getFileName() {
        const now = new Date();
        let fileName = `config-${now.getFullYear()}`;
        fileName += `${('0' + (now.getMonth() + 1).toString().slice(-2))}`;
        fileName += `${('0' + now.getDate()).slice(-2)}-`;
        fileName += `${('0' + now.getHours()).slice(-2)}`;
        fileName += `${('0' + now.getMinutes()).slice(-2)}`;
        fileName += `${('0' + now.getSeconds()).slice(-2)}.yaml`;
        return fileName;
    }

    // Endpoint success callback.
    successCallback(response) {
        this.setRedirect();
    }

    // Endpoint error callback.
    errorCallback(error, message) {
        this.child.current.customMessage("error", message);
    }

    // Renders the list of IoT controllers.
    renderControllers() {
        if (this.state.controllersLoaded) {
            return this.controllerList.map((controller) => {
                return (
                    <MenuItem
                        data-controller-key={controller.iot_controller_id}
                        value={controller.iot_controller_name}
                        key={controller.iot_controller_id}
                        name={controller.iot_controller_id}>
                        {controller.iot_controller_name}
                    </MenuItem>
                );
            });
        }
        else {
            return null;
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <Container className={classes.root}>
                {this.renderRedirect()}
                <EndpointSnackBar ref={this.child} />
                <Typography className={classes.createTestTitle} variant="h4" component="h1">Create Test</Typography>
                <Typography className={classes.requiredText} variant="h6" component="h5">*: Required</Typography>
                <Typography className={classes.formField} variant="h6" component="h3">Test Name *</Typography>
                <TextField
                    className={classes.createTestField}
                    id="create-test-name"
                    variant="outlined"
                    fullWidth={true}
                    required
                    onChange={this.onNameChange.bind(this)}
                    error={this.state.nameError}
                    helperText={this.state.nameErrorMsg}
                    inputProps={{ style: { left: 15, top: 5, position: "relative" } }}
                />
                <Typography className={classes.formField} variant="h6" component="h3">Test Description</Typography>
                <TextField
                    className={classes.createTestField}
                    id="create-test-description"
                    variant="outlined"
                    fullWidth={true}
                    multiline
                    rows="5"
                    rowsMax="5"
                    placeholder="Brief description of your test. Maximum 200 characters."
                    onChange={this.onDescChange.bind(this)}
                    error={this.state.descError}
                    helperText={this.state.descErrorMsg}
                />
                <Typography className={classes.formField} variant="h6" component="h3">Test Duration (seconds) *</Typography>
                <TextField
                    className={classes.createTestField}
                    id="create-test-duration"
                    variant="outlined"
                    fullWidth={true}
                    required
                    onChange={this.onDurationChange.bind(this)}
                    error={this.state.durationError}
                    helperText={this.state.durationErrorMsg}
                    inputProps={{ style: { left: 15, top: 5, position: "relative" } }}
                />
                <Typography className={classes.formField} variant="h6" component="h3">IoT Controller</Typography>
                <Select
                    IconComponent={() => (
                        <IconButton aria-label="clear" onClick={this.removeController.bind(this)}>
                            <ClearIcon />
                        </IconButton>
                    )}
                    className="selectBox"
                    native={false}
                    renderValue={() => {if (this.state.controller === "") {return "None Selected"} else {return this.state.controller}}}
                    displayEmpty={true}
                    error={this.state.controllerError}
                    name={this.state.controllerID}
                    value={this.state.controller}
                    onChange={this.onControllerChange.bind(this)}>
                    {this.renderControllers()}
                </Select>
                {
                    this.state.images.map((image, index) => {
                        return (
                            <Grid className={classes.imageSelectionArea} key={index} container spacing={1}>
                                <Grid item xs={12} sm={4}>
                                    <Typography className={classes.formField} variant="h6" component="h4">Image ID</Typography>
                                    <TextField
                                        className={classes.imageField}
                                        id={`create-test-imageid-${index}`}
                                        variant="outlined"
                                        fullWidth={false}
                                        required
                                        onChange={this.onImageIdChange.bind(this, index)}
                                        error={image.error}
                                        helperText={image.msg}
                                        inputProps={{ style: { left: 15, top: 5, position: "relative" } }}
                                        value={image.id}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={8}>
                                    <Typography className={classes.formField} variant="h6" component="h4">Motes</Typography>
                                    { this.renderMoteList(classes, image, index) }
                                </Grid>
                                <IconButton onClick={this.removeImage.bind(this, index)} className={classes.removeImageButton} aria-label="delete">
                                    <DeleteIcon fontSize="small" />
                                </IconButton>
                                <Grid item xs={12}>
                                    <Button onClick={this.selectAllMotes.bind(this, index)} className={`${classes.smallButton} ${classes.selectButton}`}>Select All</Button>
                                    <Button onClick={this.deselectAllMotes.bind(this, index)} className={`${classes.smallButton} ${classes.deselectButton}`}>Deselect All</Button>
                                </Grid>
                            </Grid>
                        );
                    })
                }
                <EndpointSnackBar ref={this.child} />
                <CustomButton className={classes.addImageButton} onClick={this.addImage.bind(this)}>Add another Image</CustomButton>
                <CustomButton className={classes.createTestButton} onClick={this.createTestSubmit.bind(this)}>Schedule Test</CustomButton>
            </Container>
        );
    }

    // Renders the list of checkbox motes per image.
    renderMoteList(classes, image, index) {
        return this.state.moteIDList.map((value, moteIndex) => {
            const timedOut = this.state.moteList[value].status === constants.mote_timeout_status;

            return (
                <FormControlLabel
                    key={moteIndex}
                    className={classes.checkbox}
                    control={<Checkbox checked={image.motes[moteIndex]} onChange={this.onMoteChange.bind(this, index, moteIndex)} value={value} disabled={timedOut}/>}
                    label={value}
                />
            )
        })
    }
}

CreateTestTab.propTypes = {
    classes: PropTypes.object.isRequired,
    userID: PropTypes.number.isRequired
};

export default withStyles(useStyles)(CreateTestTab);