/*
    TeamMember.js

    Used to display one team member who worked on Lucidlab in a visually pleasing format.
    The team member that it displays is based on the prop that is passed into it.
 */

import React from 'react';
import PropTypes from 'prop-types';
import '../App.css';
import {withStyles} from '@material-ui/core/styles';


const useStyles = ({
    container: {
        marginRight: "105px",
        minWidth: "200px",
        marginTop: "25px",
        position: "relative",
        float: "left",
        height: "75px",
        width: "17%",
        borderRadius: "5px",
        backgroundColor: "white",
        boxShadow: "0 1px 2px 1px rgba(133,133,133,0.7)",
        cursor: "default",
        "&:hover": {
            backgroundColor: "#F0F0F0",
        },
    },
    title: {
        position: "absolute",
        fontSize: "13px",
        marginLeft: "20%",
        bottom: "19px",
        maxWidth: "inherit",
        textAlign: "left",
    },
    line: {
        width: "20%",
        height: "2px",
        border: "none",
        color: "#4ba173",
        backgroundColor: "#4ba173",
        position: "absolute",
        marginLeft: "22%",
        top: "40px",
    },
    name: {
        position: "absolute",
        fontSize: "15px",
        fontWeight: "bold",
        marginLeft: "20%",
        top: "35px",
    },
    image: {
        position: "absolute",
        userDrag: "none",
        userSelect: "none",
        width: "auto",
        height: "96%",
        top: "3px",
        left: "-22%",
        borderRadius: "45px",
        border: "2px solid #F0F0F0",
    },
});

class TeamMember extends React.Component {

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.container}>
                <p className={classes.title}>{this.props.member.title}</p>
                <hr className={classes.line} />
                <p className={classes.name}>{this.props.member.name}</p>
                <img className={classes.image} src={this.props.member.img} alt={this.props.member.name}/>
            </div>
        );
    }
}

TeamMember.propTypes = {
    member: PropTypes.object.isRequired,
};

export default withStyles(useStyles)(TeamMember);