/*
    SignIn.js

    This component is used as a modal to facilitate a user signing in from anywhere in LucidLab.
    It accepts a username and a password combination which is sent to the backend server for checking.
    It allows the user to press the enter button to sign in, to be consistent with similar controls
    elsewhere on the Internet.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import * as endpoints from '../constants/endpoints';
import Endpoint from '../Endpoint';
import CustomButton from './CustomButton';

// Slide Animation.
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = theme => ({
    root: {},
    signUpBtn: {
        background: "linear-gradient(45deg, #08C27C 30%, #06CC86 90%)",
        borderRadius: "20px",
        lineHeight: 0,
    },
    cancelBtn: {
        background: "#f3513f",
        lineHeight: 0,
        paddingTop: "1.5em",
        paddingBottom: "1.5em",
        '&:hover': {
            background: '#f67d6f'
        },
    },
    submitBtn: {
        background: "#6BC193",
        lineHeight: 0,
        paddingTop: "1.5em",
        paddingBottom: "1.5em",
        '&:hover': {
            background: '#84CBA5'
        },
    },
    signInField: {
        marginTop: "1em!important",
        marginBottom: "2em!important",
    },
    spacer: {
        marginTop: "1em",
        marginBottom: "3em",
    }
});

class SignIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            username: "",
            password: "",
            usernameError: false,
            passwordError: false,
            usernameErrorMsg: "",
            passwordErrorMsg: "",
        }
    }

    // Opens the Dialog.
    openDialog() {
        this.setState({ open: true });
    }

    // Closes the dialog.
    closeDialog() {
        this.setState({ open: false });
    }

    // Username input change event.
    onUsernameChange(e) {
        this.setState({ username: e.target.value });
    }

    // Password input change event.
    onPasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    // Validates the fields in the sign in form.
    validateForm() {
        let passValidation = true;

        // Empty Username.
        if (this.state.username.length === 0) {
            this.setState({ usernameError: true, usernameErrorMsg: "Username is required." });
            passValidation = false;
        }
        else {
            this.setState({ usernameError: false, usernameErrorMsg: "" });
        }

        // Empty Password.
        if (this.state.password.length === 0) {
            this.setState({ passwordError: true, passwordErrorMsg: "Password is required." });
            passValidation = false;
        }
        else {
            this.setState({ passwordError: false, passwordErrorMsg: "" });
        }

        // Passes all validation tests.
        if (passValidation) {
            this.submit();
        }
    }

    // Sends the sign up request to the endpoint.
    submit() {
        // Constructs the form data.
        const data = new FormData();
        data.append("username", this.state.username);
        data.append("password", this.state.password);

        // Creates a new user.
        const createUserEndpoint = new Endpoint(endpoints.TestLogin, this.successCallback.bind(this), this.errorCallback.bind(this));
        createUserEndpoint.post(data);
    }

    // Endpoint success callback.
    successCallback(response) {
        this.closeDialog();
        this.setState({ username: "", password: "" });
        this.props.action(response.data.user_id); // Sets the user as signed in.
    }

    // Endpoint error callback.
    errorCallback(error, message) {
        const msg = "Incorrect Username and/or Password.";

        this.setState({ usernameError: true, usernameErrorMsg: msg, passwordError: true, passwordErrorMsg: msg });
    }

    // On Enter key press validate form.
    keyPress(e) {
        if (e.keyCode === 13) {
            this.validateForm();
        }
    }

    render() {
        const { classes } = this.props;

        if (!this.props.signedIn) {
            return (
                <div>
                    <CustomButton className={classes.signUpBtn} onClick={this.openDialog.bind(this)}>
                        Sign In
                    </CustomButton>
                    <Dialog
                        open={this.state.open}
                        TransitionComponent={Transition}
                        keepMounted
                        onClose={this.closeDialog.bind(this)}
                        aria-labelledby="alert-dialog-slide-title"
                        aria-describedby="alert-dialog-slide-description"
                    >
                        <DialogTitle id="alert-dialog-slide-title">{"Existing User Sign In"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText className={classes.spacer} id="alert-dialog-slide-description">
                                Sign in to LucidLab to gain access to our testbed.
                            </DialogContentText>

                            <Typography className={classes.formField} variant="h6" component="h3">Username *</Typography>
                            <TextField
                                className={classes.signInField}
                                id="signin-username"
                                variant="outlined"
                                fullWidth={true}
                                required
                                onChange={this.onUsernameChange.bind(this)}
                                error={this.state.usernameError}
                                helperText={this.state.usernameErrorMsg}
                                inputProps={{ style: { left: 15, top: 5, position: "relative" } }}
                                onKeyDown={this.keyPress.bind(this)}
                            />

                            <Typography className={classes.formField} variant="h6" component="h3">Password *</Typography>
                            <TextField
                                className={classes.signInField}
                                id="signin-password"
                                variant="outlined"
                                fullWidth={true}
                                required
                                type="password"
                                onChange={this.onPasswordChange.bind(this)}
                                error={this.state.passwordError}
                                helperText={this.state.passwordErrorMsg}
                                inputProps={{ style: { left: 15, top: 5, position: "relative" } }}
                                onKeyDown={this.keyPress.bind(this)}
                            />

                        </DialogContent>
                        <DialogActions>
                            <CustomButton className={classes.cancelBtn} onClick={this.closeDialog.bind(this)}>Cancel</CustomButton>
                            <CustomButton className={classes.submitBtn} onClick={this.validateForm.bind(this)}>Sign In</CustomButton>
                        </DialogActions>
                    </Dialog>
                </div>
            );
        }
        else {
            return null;
        }
    }
}

SignIn.propTypes = {
    classes:  PropTypes.object.isRequired,
    signedIn: PropTypes.bool.isRequired,
    action:   PropTypes.func.isRequired,
};

export default withStyles(useStyles)(SignIn);