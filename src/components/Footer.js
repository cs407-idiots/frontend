/*
    Footer.js

    Stores the fixed footer used by every page in the application, with a filled background and a
    static copyright message.
 */

import React from 'react';
import '../App.css';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';


const useStyles = theme => ({
    root: {
        top: "auto",
        bottom: 0,
        backgroundColor: "#FFBDBD",
        fontSize: "10px",
        height: 25,
        verticalAlign: "middle",
    },
    copyright: {
        lineHeight: 2.5,
    }
});


class Footer extends React.Component {
    render() {
        const { classes } = this.props;

        return (
            <AppBar className={classes.root} position="fixed">
                <span className={classes.copyright}>© Copyright LucidLab 2020</span>
            </AppBar>
        );
    }
}

Footer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(useStyles)(Footer);