/*
    UploadIoTController.js

    This component is used to render the tab panel to allow a user to upload an IoT Controller to the
    application in python formats. Fields are included to accept a name, description, and validation
    is performed on these fields when the user attempts to submit the form, before attempting to send
    to the back-end. After uploading an IoT Controller, the user is redirected to the Manage IoT Controllers
    tab of the Uploads page.

    By making use of the drop-zone's 'acceptedFiles' attribute, we can effectively restrict which file
    types the user is able to upload.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import CustomButton from './CustomButton';
import {DropzoneArea} from 'material-ui-dropzone';
import EndpointSnackbar from './EndpointSnackbar';
import Endpoint from '../Endpoint';
import * as endpoints from '../constants/endpoints';
import * as constants from '../constants/';
import TextField from "@material-ui/core/TextField";

// File Upload Styling.
const useStyles = ({
    root: {
        '& .MuiTextField-root': {
            marginTop: "0.5em",
            marginBottom: "1.2em",
        },
        '& .selectBox': {
            width: "100%",
            borderStyle: "solid",
            borderWidth: "1px",
            borderColor: "lightgray",
            borderRadius: "3px",
            marginBottom: "15px"
        },
        height: "auto"
    },
    dropZone: {
        minHeight: "200px",
    },
    submitButton: {
        background: "#6BC193",
        height: 48,
        paddingTop: "2em",
        paddingBottom: "2em",
        lineHeight: 0,
        paddingLeft: "3em",
        paddingRight: "3em",
        marginBottom: "20px",
        '&:hover': {
            background: '#84CBA5'
        }
    },
    uploadControllerTitle: {
        marginTop: "1em",
        marginBottom: "1em",
    },
    uploadControllerField: {
        marginTop: "3em",
        marginBottom: "3em",
    }
});

// Draggable File Upload Component.
class UploadIoTController extends React.Component {
    constructor(props) {
        super(props);
        this.child = React.createRef();
        this.state = {
            name: "",
            desc: "",
            files: [],
            nameError: false,
            descError: false,
            nameErrorMsg: "",
            descErrorMsg: ""
        };
    }

    // Name text input change event.
    onNameChange(e) {
        this.setState({name: e.target.value});
    }

    // Description text input change event.
    onDescChange(e) {
        this.setState({desc: e.target.value});
    }

    // File input change event.
    handleChange(files) {
        this.setState({files: files});
    }

    // Alert closed by user or automatically after 5 seconds.
    closeAlert() {
        this.setState({alertOpen: false});
    }

    // Name validation.
    validateName() {
        const maxLengthName = 50;

        if (this.state.name.length === 0) {
            this.setState({nameError: true, nameErrorMsg: "Controller name is required."});
            return false;
        } else if (this.state.name.length > maxLengthName) {
            const msg = "Controller name exceeds max length of " + maxLengthName + " characters.";
            this.setState({nameError: true, nameErrorMsg: msg});
            return false
        } else {
            this.setState({nameError: false, nameErrorMsg: ""});
            return true
        }
    }

    // Description validation.
    validateDesc() {
        const maxLengthDesc = 200;

        if (this.state.desc.length > maxLengthDesc) {
            const msg = "Controller description exceeds max length of " + maxLengthDesc + " characters.";
            this.setState({descError: true, descErrorMsg: msg});
            return false;
        } else {
            this.setState({descError: false, descErrorMsg: ""});
            return true;
        }
    }

    // Controller Validation.
    validateController() {
        if (this.state.files.length === 0) {
            this.child.current.customMessage("warning", "Failed to upload. No controller file has been selected.");
            return false;
        }

        return true;
    }

    // Submits the File.
    async submit() {
        let passValidation = true;

        // Validation of text inputs.
        if (!this.validateName()) {
            passValidation = false;
        }
        if (!this.validateDesc()) {
            passValidation = false;
        }
        if (!this.validateController()) {
            passValidation = false;
        }

        // Post to endpoint.
        if (passValidation) {
            const data = new FormData();
            const userID = this.props.userID;

            data.append("user_id", userID);
            data.append("name", this.state.name);
            data.append("description", this.state.desc);
            data.append("controller", this.state.files[0]);

            const uploadControllerEndpoint = new Endpoint(
                endpoints.UploadController,
                this.successCallback.bind(this),
                this.errorCallback.bind(this)
            );

            uploadControllerEndpoint.uploadFile(data);
        }
    }

    // Endpoint success callback.
    successCallback(response) {
        this.child.current.customMessage("success", response.data.message);
        // Key incremented to force re-render of the dropzone.
        // Clears the selected files.
        this.setState({files: [], key: this.state.key + 1});
        this.props.switchTab(constants.upload_page_tab_index["Manage-IoT-Controllers"]);
    }

    // Endpoint error callback.
    errorCallback(error, msg) {
        this.child.current.customMessage("error", msg);
        // Clears the erroneous controller file.
        this.setState({files: [], key: this.state.key + 1});
    }

    renderFileTypes() {
        let fileTypesString = "";
        constants.accepted_controller_file_types.map(type => fileTypesString += type + " ");
        return fileTypesString;
    }

    render() {
        const {classes} = this.props;

        return (
            <Container className={classes.root}>
                <Typography className={classes.uploadControllerTitle} variant="h4" component="h1">Upload IoT
                    Controller</Typography>
                <TextField
                    className={classes.uploadControllerField}
                    id="upload-controller-name"
                    label="Controller Name"
                    variant="outlined"
                    fullWidth={true}
                    required
                    onChange={this.onNameChange.bind(this)}
                    error={this.state.nameError}
                    helperText={this.state.nameErrorMsg}
                    inputProps={{style: {left: 15, top: 5, position: "relative"}}}
                />
                <TextField
                    className={classes.uploadControllerField}
                    id="upload-controller-desc"
                    label="Controller Description"
                    variant="outlined"
                    fullWidth={true}
                    multiline
                    rows="5"
                    rowsMax="5"
                    onChange={this.onDescChange.bind(this)}
                    error={this.state.descError}
                    helperText={this.state.descErrorMsg}
                />
                <DropzoneArea
                    key={this.state.key}
                    drop-zone-class={classes.dropZone}
                    onChange={this.handleChange.bind(this)}
                    dropzoneText={'Click or drag and drop to upload a controller file. Accepted file types are: ' + this.renderFileTypes()}
                    acceptedFiles={constants.accepted_controller_file_types}
                    filesLimit={1}
                    showFileNames={true}
                    maxFileSize={1000000}
                    height={200}
                    showPreviewsInDropzone={true}
                    useChipsForPreview={true}
                />
                <br/>
                <EndpointSnackbar ref={this.child}/>
                <CustomButton className={classes.submitButton} onClick={this.submit.bind(this)}>Upload</CustomButton>
            </Container>
        );
    }
}

// Higher-Order component.
UploadIoTController.propTypes = {
    classes:    PropTypes.object.isRequired,
    userID:     PropTypes.number.isRequired,
    switchTab:  PropTypes.func.isRequired,
};

export default withStyles(useStyles)(UploadIoTController);
