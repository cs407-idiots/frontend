/*
    SignUp.js

    This component is used as a modal to facilitate a user signing up from the navbar in LucidLab.
    It takes an email, username and a password combination which is sent to the backend server for
    validation. It allows the user to press the enter button to sign in, to be consistent with similar
    controls elsewhere on the Internet.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import * as endpoints from '../constants/endpoints';
import Endpoint from '../Endpoint';
import CustomButton from './CustomButton';


// Slide Animation.
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = theme => ({
    root: {

    },
    signUpBtn: {
        background: "linear-gradient(45deg, #08C27C 30%, #06CC86 90%)",
        borderRadius: "20px",
        lineHeight: 0,
    },
    cancelBtn: {
        background: "#f3513f",
        lineHeight: 0,
        paddingTop: "1.5em",
        paddingBottom: "1.5em",
        '&:hover': {
            background: '#f67d6f'
        },
    },
    submitBtn: {
        background: "#6BC193",
        lineHeight: 0,
        paddingTop: "1.5em",
        paddingBottom: "1.5em",
        '&:hover': {
            background: '#84CBA5'
        },
    },
    signUpField: {
        marginTop: "1em!important",
        marginBottom: "2em!important",
    },
    spacer: {
        marginTop: "1em",
        marginBottom: "3em",
    }
});


class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            username: "",
            email: "",
            password: "",
            usernameError: false,
            emailError: false,
            passwordError: false,
            usernameErrorMsg: "",
            emailErrorMsg: "",
            passwordErrorMsg: "",
        }
    }

    // Opens the Dialog.
    openDialog() {
        this.setState({ open: true });
    }

    // Closes the dialog.
    closeDialog() {
        this.setState({ open: false });
    }

    // Username input change event.
    onUsernameChange(e) {
        this.setState({ username: e.target.value });
    }

    // Email input change event.
    onEmailChange(e) {
        this.setState({ email: e.target.value });
    }

    // Password input change event.
    onPasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    // Validates the fields in the sign up form.
    validateForm() {
        let passValidation = true;
        const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const passwordMinLength = 8;

        // Empty Username.
        if (this.state.username.length === 0) {
            this.setState({ usernameError: true, usernameErrorMsg: "Username is required." });
            passValidation = false;
        }
        else {
            this.setState({ usernameError: false, usernameErrorMsg: "" });
        }

        // Empty Email.
        if (this.state.email.length === 0) {
            this.setState({ emailError: true, emailErrorMsg: "Email is required." });
            passValidation = false;
        }
        // Email Regex.
        else if (!emailRegex.test(this.state.email.toLowerCase())) {
            this.setState({ emailError: true, emailErrorMsg: "Email is invalid." });
            passValidation = false;
        }
        else {
            this.setState({ emailError: false, emailErrorMsg: "" });
        }

        // Empty Password.
        if (this.state.password.length === 0) {
            this.setState({ passwordError: true, passwordErrorMsg: "Password is required." });
            passValidation = false;
        }
        // Too Short Password.
        else if (this.state.password.length < passwordMinLength) {
            this.setState({ passwordError: true, passwordErrorMsg: "Password must be at least " + passwordMinLength.toString() + " characters long." });
            passValidation = false;
        }
        else {
            this.setState({ passwordError: false, passwordErrorMsg: "" });
        }

        // Passes all validation tests.
        if (passValidation) {
            this.submit();
        }
    }

    // Sends the sign up request to the endpoint.
    submit() {
        // Constructs the form data.
        const data = new FormData();
        data.append("username", this.state.username);
        data.append("email", this.state.email);
        data.append("password", this.state.password);

        // Creates a new user.
        const createUserEndpoint = new Endpoint(endpoints.CreateUser, this.successCallback.bind(this), this.errorCallback.bind(this));
        createUserEndpoint.post(data);
    }

    // Endpoint success callback.
    successCallback(response) {
        this.closeDialog();
        this.setState({ username: "", email: "", password: "" });
        this.props.action(response.data.id); // Sets the user as signed in.
    }

    // Endpoint error callback.
    errorCallback(error, message) {
        this.setState({
            usernameError: true,
            emailError: true,
            passwordError: true,
            usernameErrorMsg: message,
            emailErrorMsg: message,
            passwordErrorMsg: message
        })
    }

    // On Enter key press validate form.
    keyPress(e) {
        if (e.keyCode === 13) {
            this.validateForm();
        }
    }

    render() {
        const { classes } = this.props;

        if (!this.props.signedIn) {
            return (
                <div>
                    <CustomButton className={classes.signUpBtn} onClick={this.openDialog.bind(this)}>
                        Sign Up
                    </CustomButton>
                    <Dialog
                        open={this.state.open}
                        TransitionComponent={Transition}
                        keepMounted
                        onClose={this.closeDialog.bind(this)}
                        aria-labelledby="alert-dialog-slide-title"
                        aria-describedby="alert-dialog-slide-description"
                    >
                        <DialogTitle id="alert-dialog-slide-title">{"New User Registration"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText className={classes.spacer} id="alert-dialog-slide-description">
                                Sign up to LucidLab to gain access to our fantastic testbed.
                            </DialogContentText>

                            <Typography className={classes.formField} variant="h6" component="h3">Username *</Typography>
                            <TextField
                                className={classes.signUpField}
                                id="signup-username"
                                variant="outlined"
                                fullWidth={true}
                                required
                                onChange={this.onUsernameChange.bind(this)}
                                error={this.state.usernameError}
                                helperText={this.state.usernameErrorMsg}
                                inputProps={{ style: { left: 15, top: 5, position: "relative" } }}
                                onKeyDown={this.keyPress.bind(this)}
                            />

                            <Typography className={classes.formField} variant="h6" component="h3">Email *</Typography>
                            <TextField
                                className={classes.signUpField}
                                id="signup-email"
                                variant="outlined"
                                fullWidth={true}
                                required
                                onChange={this.onEmailChange.bind(this)}
                                error={this.state.emailError}
                                helperText={this.state.emailErrorMsg}
                                inputProps={{ style: { left: 15, top: 5, position: "relative" } }}
                                onKeyDown={this.keyPress.bind(this)}
                            />

                            <Typography className={classes.formField} variant="h6" component="h3">Password *</Typography>
                            <TextField
                                className={classes.signUpField}
                                id="signup-password"
                                variant="outlined"
                                fullWidth={true}
                                required
                                type="password"
                                onChange={this.onPasswordChange.bind(this)}
                                error={this.state.passwordError}
                                helperText={this.state.passwordErrorMsg}
                                inputProps={{ style: { left: 15, top: 5, position: "relative" } }}
                                onKeyDown={this.keyPress.bind(this)}
                            />

                        </DialogContent>
                        <DialogActions>
                            <CustomButton className={classes.cancelBtn} onClick={this.closeDialog.bind(this)}>Cancel</CustomButton>
                            <CustomButton className={classes.submitBtn} onClick={this.validateForm.bind(this)}>Sign Up</CustomButton>
                        </DialogActions>
                    </Dialog>
                </div>
            );
        }
        else {
            return null;
        }
    }
}

SignUp.propTypes = {
    classes:  PropTypes.object.isRequired,
    signedIn: PropTypes.bool.isRequired,
    action:   PropTypes.func.isRequired,
};

export default withStyles(useStyles)(SignUp);