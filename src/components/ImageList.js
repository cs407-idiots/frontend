/*
    ImageList.js

    This page is used to list the images stored that are associated to the user. The table gives the
    user options to download the image to check its contents, or delete the image from both the table
    and the back-end server.

    It displays when the image was last used to the user because, in line with our data retention policy,
    images are automatically deleted after a set time period.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import MaterialTable from 'material-table';
import EndpointSnackbar from './EndpointSnackbar';
import Endpoint from '../Endpoint';

import * as endpoints from '../constants/endpoints';

const useStyles = ({
  root: {
    marginBottom: "1em",
  },
  title: {
    fontSize: "2rem",
    textAlign: "left",
    fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif",
  },
});

class ImageList extends React.Component {
  image_name;
  image_id;
  image_description;
  constructor(props) {
    super(props);
    this.child = React.createRef();
    this.state = {
      table: []
    };
  }
  componentDidMount() {
    this.fetchImages();
  }

  // Fetches the images from the endpoint.
  fetchImages = async () => {
    const userID = this.props.userID;
    const data = {"user_id": userID};

    const loadImagesEndpoint = new Endpoint(
      endpoints.GetImages,
      this.loadSuccessCallback.bind(this),
      this.errorCallback.bind(this)
    );
    await loadImagesEndpoint.asyncGet(data);
  };

  // Removes an image from the table
  removeImage(rowData) {
      // eslint-disable-next-line no-restricted-globals
      const confirmation = confirm("Are you sure you want to delete this image?");
      if (confirmation) {
          const image_id = parseInt(rowData.id);
          const data = {"image_id": image_id};

          const removeImageEndpoint = new Endpoint(
              endpoints.DeleteImage,
              this.removeSuccessCallback.bind(this),
              this.errorCallback.bind(this)
          );
          removeImageEndpoint.get(data);

          const index = rowData.tableData.id;
          let table = this.state.table;
          table.splice(index, 1);
          this.setState({ table });
      }
    }

    // Downloads an image from the table
    downloadImage = async (rowData) => {
        const image_id = rowData.id;

        const data = {"image_id": image_id};

        const downloadImageEndpoint = new Endpoint(
            endpoints.DownloadImage,
            this.downloadImageSuccessCallback.bind(this),
            this.errorCallback.bind(this)
        );

        let response = {};

        // Retrieves the entire response such that body and headers can be analysed
        await downloadImageEndpoint.asyncGet(data).then(function(res) {
            response = res;
        });

        // Extracts headers and file data
        const headers = response.headers;
        const fileObj = response.data;

        // Extracts the filename from the content-disposition header
        const filename = "image-" + headers['content-disposition'].split("=")[1];

        // Creates a file by converting the file object into a Blob
        const file = new Blob([fileObj], {type: 'application/octet-stream'});
        // Creates an anchor node for the duration of a download
        // Allows the specification of a filename, set to iot_controller.py
        // Reliably starts the download automatically
        let downloadAnchorNode = document.createElement('a');
        // Creates an object URL with which to reference the file on the site
        downloadAnchorNode.setAttribute("href", URL.createObjectURL(file));
        downloadAnchorNode.setAttribute("download",  filename);
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    };

    // Load Endpoint success callback.
    loadSuccessCallback(response) {
        const images = response.data.images;
        const tableData = images.map(image =>  {
            return {  name: image.image_name,
                id: image.image_id.toString(),
                desc: image.image_description,
                lastUsed: new Date(image.date_last_used) };
        });

        this.setState({
            table: tableData
        })
    }

    // Download Image success callback.
    downloadImageSuccessCallback = async (response) => {
        // Returns full response to enable deconstruction of headers as well as body
        return response;
    };

    // Remove Endpoint success callback.
    removeSuccessCallback(response) {
        // Intentionally Empty
    }

    // Endpoint error callback.
    errorCallback(error, msg) {
        this.child.current.customMessage("error", msg);
    }

    render() {
        const { classes } = this.props;

        return (
            <Container className={classes.root} maxWidth="lg">
              <h1 className={classes.title}>Manage Images</h1>

              <MaterialTable
                  columns={[
                    { title: 'ID', field: 'id', type: 'numeric', defaultSort: 'desc'},
                    { title: 'Name', field: 'name'},
                    { title: 'Description', field: 'desc', sorting: false},
                    { title: 'Last Used', field: 'lastUsed', type: 'datetime'},
                  ]}
                  data={this.state.table}
                  actions={[
                    {
                      icon: 'get_app',
                      tooltip: 'Download Image',
                      onClick: (event, rowData) => this.downloadImage(rowData)
                    },
                    {
                      icon: 'delete',
                      tooltip: 'Remove Image',
                      onClick: (event, rowData) => this.removeImage(rowData)
                    }
                  ]}
                  options={{
                    sorting: true,
                    search: false,
                    showTitle: false,
                    actionsColumnIndex: -1
                  }}
              />

            <EndpointSnackbar ref={this.child} />
            </Container>
        );
    }
}

ImageList.propTypes = {
  classes: PropTypes.object.isRequired,
    userID: PropTypes.number.isRequired
};

export default withStyles(useStyles)(ImageList);