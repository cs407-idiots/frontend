/*
    UploadTest.js

    This component is used to render the tab panel to allow a user to upload a test config file to the
    application in YAML formats. When the user submits their config file, they are then redirected to
    the scheduled tests page.

    By making use of the drop-zone's 'acceptedFiles' attribute, we can effectively restrict which file
    types the user is able to upload.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import CustomButton from './CustomButton';
import {DropzoneArea} from 'material-ui-dropzone';
import EndpointSnackbar from './EndpointSnackbar';
import Endpoint from '../Endpoint';
import { blobToSHA1 } from 'file-to-sha1';
import * as endpoints from '../constants/endpoints';
import { Redirect } from 'react-router-dom'

// File Upload Styling.
const useStyles = theme => ({
    root: {},
    dropZone: {
        height: 400,
    },
    submitButton: {
        background: "#6BC193",
        height: 48,
        paddingTop: "2em",
        paddingBottom: "2em",
        paddingLeft: "3em",
        paddingRight: "3em",
        marginBottom: "20px",
        lineHeight: 0,
        '&:hover': {
            background: '#84CBA5'
        }
    },
    uploadTestTitle: {
        marginTop: "1em",
        marginBottom: "1em",
    },
});


// Draggable File Upload Component.
class UploadTest extends React.Component {
    constructor(props){
        super(props);
        this.child = React.createRef();
        this.state = {
            files: [],
            redirect: false
        };
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        })
    };

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/metrics' />
        }
    };

    // File input change event.
    handleChange(files) {
        this.setState({ files: files });
    }

    // Submits the File.
    async submit() {
        if (this.state.files.length === 0) {
            this.child.current.customMessage("warning", "Failed to upload. No test file has been selected.");
        }
        else {
            const data = new FormData();
            const checksum = await blobToSHA1(this.state.files[0]);
            const userID = this.props.userID;

            data.append("user_id", userID);
            data.append("yaml", this.state.files[0]);
            data.append("checksum", checksum);
            const uploadTestEndpoint = new Endpoint(endpoints.UploadConfig, this.successCallback.bind(this), this.errorCallback.bind(this));
            uploadTestEndpoint.uploadFile(data);
        }
    }

    // Endpoint success callback.
    successCallback(response) {
        this.child.current.customMessage("success", response.data.message);
        // Key incremented to force re-render of the dropzone.
        // Clears the selected files.
        this.setState({ files: [], key: this.state.key + 1 });
        this.setRedirect();
    }

    // Endpoint error callback.
    errorCallback(error, msg) {
        this.child.current.customMessage("error", msg);
        // Clears the erroneous configuration.
        this.setState({files: [], key: this.state.key + 1});
    }

    render() {
        const { classes } = this.props;

        return (
            <Container className={classes.root}>
                {this.renderRedirect()}
                <Typography className={classes.uploadTestTitle} variant="h4" component="h1">Upload Test</Typography>
                <DropzoneArea
                    key={this.state.key}
                    className={classes.dropZone}
                    onChange={this.handleChange.bind(this)}
                    dropzoneText='Click or drag and drop to upload a test file. Accepted file types are: YAML.'
                    acceptedFiles={[".yaml", "text/yaml", "text/x-yaml", "application/yaml", "application/x-yaml"]}
                    filesLimit={1}
                    showFileNames={true}
                    maxFileSize={1000000}
                    height={400}
                    showPreviewsInDropzone={true}
                    useChipsForPreview={true}
                />
                <br />
                <EndpointSnackbar ref={this.child} />
                <CustomButton className={classes.submitButton} onClick={this.submit.bind(this)}>Schedule Test</CustomButton>
            </Container>
        );
    }
}

// Higher-Order component.
UploadTest.propTypes = {
    classes: PropTypes.object.isRequired,
    userID: PropTypes.number.isRequired
};

export default withStyles(useStyles)(UploadTest);
