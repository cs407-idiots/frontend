/*
    IoTDevices.js

    This page is used to list the IoT Devices that are currently connected to the test bed. It displays
    to the user both the device's openHAB ID and the device name.
 */

import React from 'react';
import '../App.css';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Endpoint from '../Endpoint';
import MaterialTable from 'material-table';
import * as endpoints from '../constants/endpoints';
import Chip from '@material-ui/core/Chip';
import CachedIcon from '@material-ui/icons/Cached';

// Current date.
function getCurrentDate(date) {
    return ((date.getDate() < 10) ? "0" : "") + date.getDate() + "/" +(((date.getMonth() + 1) < 10) ? "0" : "") + (date.getMonth() + 1) + "/" + date.getFullYear();
}

// Current time.
function getCurrentTime(date) {
    return ((date.getHours() < 10) ? "0" : "") + date.getHours() + ":" + ((date.getMinutes() < 10) ? "0" : "") + date.getMinutes() + ":" + ((date.getSeconds() < 10) ? "0" : "") + date.getSeconds();
}

const useStyles = ({
    root: {
        marginBottom: "1em"
    },
    title: {
        marginTop: "1em",
        marginBottom: "1em",
    },
    lastUpdated: {
        textAlign: "right",
        fontSize: "0.7125rem",
        paddingTop: "1px",
        float: "right",
        marginTop: "1.5em",
    },
});


class IoTDevices extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            table: [],
            lastSync: "Last Updated: Never",
        };

        this.getIoTDevices = this.getIoTDevices.bind(this);
        this.getDevicesSuccessCallback = this.getDevicesSuccessCallback.bind(this);
        this.getDevicesErrorCallback = this.getDevicesErrorCallback.bind(this);

        this.getIoTDevices();
    }

    // Gets the available IoT devices.
    getIoTDevices() {
        const getDevicesEndpoint = new Endpoint(endpoints.GetAvailableIoTDevices, this.getDevicesSuccessCallback, this.getDevicesErrorCallback);
        getDevicesEndpoint.get();
    }

    getDevicesSuccessCallback(response) {
        let devices = [];

        for (let key in response.data) {
            // Checks if the key is defined in the object itself.
            if (response.data.hasOwnProperty(key)) {
                devices.push({ key: key, name: response.data[key]});
            }
        }

        const newDate = new Date();
        const datetime = "Last Updated: " + getCurrentDate(newDate) + " @ " + getCurrentTime(newDate);

        this.setState({ table: devices, lastSync: datetime });
    }

    getDevicesErrorCallback(error, message) {
        console.log(error);
        console.log(message);
        const newDate = new Date();
        const datetime = "Last Updated: " + getCurrentDate(newDate) + " @ " + getCurrentTime(newDate);
        this.setState({ lastSync: datetime });
    }

    render() {
        const { classes } = this.props;

        return (
            <Container className={classes.root}>
                <Typography className={classes.title} variant="h4" component="h1">IoT Devices</Typography>
                <MaterialTable
                    columns={[
                        { title: "openHAB ID", field: "key" },
                        { title: "Device Name", field: "name" },
                    ]}
                    data={this.state.table}
                    options={{
                        sorting: true,
                        search: false,
                        showTitle: false,
                        actionsColumnIndex: -1
                    }}
                />
                <Chip className={classes.lastUpdated} onClick={this.getIoTDevices} variant="outlined" color="secondary" label={this.state.lastSync} icon={<CachedIcon />}/>
            </Container>
        );
    }
}

IoTDevices.propTypes = {
    classes: PropTypes.object.isRequired,
    userID: PropTypes.number.isRequired
};

export default withStyles(useStyles)(IoTDevices);
