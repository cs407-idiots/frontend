/*
    EndpointSnackbar.js

    This file serves the customised snackbar alert used throughout the site.
    For the most part, this snackbar is used to communicate endpoint success/failure callbacks,
    however it is also made use of during form validation to inform the user of specific problems
    with their entries.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

// Snackbar Styling.
const useStyles = theme => ({
    root: {}
});

// Stylised Alerts.
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

// Draggable File Upload Component.
class EndpointSnackbar extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            alertOpen: false,
            alertSeverity: "",
            alertMessage: "",
        };
    }

    // Applies a custom message to the snackbar and displays it.
    customMessage(severity, message) {
        this.setState({ alertOpen: true, alertSeverity: severity, alertMessage: message });
    }

    // Alert closed by user or automatically after 5 seconds.
    closeAlert() {
        this.setState({ alertOpen: false });
    }

    render() {
        const { classes } = this.props;

        return (
            <Snackbar
                open={this.state.alertOpen}
                onClose={this.closeAlert.bind(this)}
                autoHideDuration={8000}
                anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
            >
                <Alert className={classes.alert} severity={this.state.alertSeverity}>{this.state.alertMessage}</Alert>
            </Snackbar>
        );
    }
}

// Higher-Order component.
EndpointSnackbar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(useStyles)(EndpointSnackbar);
