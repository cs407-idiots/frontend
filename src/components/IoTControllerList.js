/*
    IoTControllerList.js

    This page is used to list the iot controllers stored that are associated to the user.
    The table gives the user options to download the controller to check its contents,
    or delete the image from both the table and the back-end server.

    It displays when the image was last used to the user because, in line with our data retention policy,
    iot controllers are automatically deleted after a set time period.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import MaterialTable from 'material-table';
import EndpointSnackbar from './EndpointSnackbar';
import Endpoint from '../Endpoint';

import * as endpoints from '../constants/endpoints';

const useStyles = ({
  root: {
    marginBottom: "1em",
  },
  title: {
    fontSize: "2rem",
    textAlign: "left",
    fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif",
  },
});

class IoTControllerList extends React.Component {
    iot_controller_name;
    iot_controller_id;
    iot_controller_description;
    iot_controllers;

    constructor(props) {
        super(props);
        this.child = React.createRef();
        this.state = {
          table: []
        };
    }

    componentDidMount() {
        this.fetchControllers();
    }

    // Fetches the controllers from the endpoint.
    fetchControllers = async () => {
        const userID = this.props.userID;
        const data = {"user_id": userID};

        const loadControllersEndpoint = new Endpoint(
          endpoints.GetControllers,
          this.loadSuccessCallback.bind(this),
          this.errorCallback.bind(this)
        );

        await loadControllersEndpoint.asyncGet(data);
    };

    // Removes an image from the table
    removeController(rowData) {
      // eslint-disable-next-line no-restricted-globals
      const confirmation = confirm("Are you sure you want to delete this IoT Controller?");
      if (confirmation) {
          const controller_id = parseInt(rowData.id);
          const data = {"iot_controller_id": controller_id};

          const removeControllerEndpoint = new Endpoint(
              endpoints.DeleteController,
              this.removeSuccessCallback.bind(this),
              this.errorCallback.bind(this)
          );
          removeControllerEndpoint.get(data);

          const index = rowData.tableData.id;
          let table = this.state.table;
          table.splice(index, 1);
          this.setState({table});
      }
    }

    // Downloads a controller from the table
    downloadController = async (rowData) => {
        const controller_id = rowData.id;

        const data = {"iot_controller_id": controller_id};

        const downloadControllerEndpoint = new Endpoint(
            endpoints.DownloadController,
            this.downloadControllerSuccessCallback.bind(this),
            this.errorCallback.bind(this)
        );

        let pyObj = "";
        let response = {};

        await downloadControllerEndpoint.asyncGet(data).then(function(res) {
            response = res;
        });

        const headers = response.headers;

        pyObj = response.data;
        // Extracts the filename from the content-disposition header
        const filename = headers['content-disposition'].split("=")[1];

        // Creates a file by converting the python object into a Blob
        const file = new Blob([pyObj], {type: 'text/x-python'});
        // Creates an anchor node for the duration of a download
        // Allows the specification of a filename, set to iot_controller.py
        // Reliably starts the download automatically
        let downloadAnchorNode = document.createElement('a');
        // Creates an object URL with which to reference the file on the site
        downloadAnchorNode.setAttribute("href", URL.createObjectURL(file));
        downloadAnchorNode.setAttribute("download",  filename);
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    };

    // Load Endpoint success callback.
    loadSuccessCallback(response) {
        const controllers = response.data.iot_controllers;
        const tableData = controllers.map(controller =>  {
            return {  name: controller.iot_controller_name,
                id: controller.iot_controller_id.toString(),
                desc: controller.iot_controller_description,
                lastUsed: new Date(controller.date_last_used) };
        });

        this.setState({
            table: tableData
        })
    }

    // Remove Endpoint success callback.
    removeSuccessCallback(response) {
        // Intentionally Empty
    }

    // Download Controller success callback, sends both headers and body
    downloadControllerSuccessCallback = async (response) => {
        return response;
    };


    // Endpoint error callback.
    errorCallback(error, msg) {
        this.child.current.customMessage("error", msg);
    }

  render() {
    const { classes } = this.props;

    return (
        <Container className={classes.root} maxWidth="lg">
          <h1 className={classes.title}>Manage IoT Controllers</h1>

          <MaterialTable
              columns={[
                { title: 'ID', field: 'id', type: 'numeric', defaultSort: 'desc'},
                { title: 'Name', field: 'name'},
                { title: 'Description', field: 'desc', sorting: false},
                { title: 'Last Used', field: 'lastUsed', type: 'datetime'},
              ]}
              data={this.state.table}
              actions={[
                {
                  icon: 'get_app',
                  tooltip: 'Download IoT Controller',
                  onClick: (event, rowData) => this.downloadController(rowData)
                },
                {
                  icon: 'delete',
                  tooltip: 'Delete IoT Controller',
                  onClick: (event, rowData) => this.removeController(rowData)
                }
              ]}
              options={{
                sorting: true,
                search: false,
                showTitle: false,
                actionsColumnIndex: -1
              }}
          />

        <EndpointSnackbar ref={this.child} />
        </Container>
    );
  }
}

IoTControllerList.propTypes = {
  classes: PropTypes.object.isRequired,
    userID: PropTypes.number.isRequired
};

export default withStyles(useStyles)(IoTControllerList);