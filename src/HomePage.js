/*
    Homepage.js

    This file serves the homepage of LucidLab, the only accessible page for a signed out user.
    It gives an overview of the application, as well as providing clear navigation buttons to traverse
    the application that are larger than those in the navbar.
    Finally, the page also shows the team members who contributed to the application, to offer a more personal
    feel to the UX.
 */

import React from 'react';
import './App.css';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import AssignmentIcon from '@material-ui/icons/Assignment';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import Icon from '@material-ui/core/Icon';
import TeamMember from "./components/TeamMember";
import * as constants from './constants/';
import Typography from '@material-ui/core/Typography';
import {Link} from 'react-router-dom';

const useStyles = makeStyles(({
  root: {
    marginTop: "2.5em",
    marginBottom: "6.8em",
  },
  splashTitle: {
    fontSize: "2rem",
    textAlign: "left",
  },
  splashText: {
    fontSize: "1rem",
    textAlign: "left",
    marginTop: "2em",
    marginBottom: "2em",
  },
  splashContent: {
    textAlign: "left",
  },
  splashDivider: {
    marginTop: "4em",
    marginBottom: "4em",
    color: "#4BA173",
  },
  splashPaper: {
    paddingTop: "1em",
    paddingBottom: "1em",
    "&:hover, &:focus": {
      backgroundColor: "#F0F0F0",
    },
  },
  linkIcon: {
    color: "#4BA173",
  },
  splashIcon: {
    paddingRight: "0.5em",
    top: "6px",
    position: "relative",
  },
  splashIconText: {
    lineHeight: "40px",
  }
}));
  

function HomePage() {

  const classes = useStyles();
  
  return (
    <Container className={classes.root} maxWidth="lg">

    <Typography variant="h3" component="h2" className={classes.splashTitle}>Welcome to LucidLab</Typography>
    
    <p className={classes.splashText}>Welcome to LucidLab, an adaptive, heterogeneous testbed for Internet
      of Things devices. After literally days of hard work, we're excited to bring you a tool built to
      integrate with a wide range of devices and sensors to allow a broad scope of application development
      for researchers. Comprehensive logging functionality is also available for the gathering of application
      debug logs.
    </p>

    <p className={classes.splashText}>Using the links below, you can create a new test, manage your uploads, and view test results and LucidLab status.</p>

    <hr className={classes.splashDivider}/>

    <Grid container spacing={2}>

      <Grid item xs={12} lg={3}>
        <Link to="/tests">
          <Paper className={classes.splashPaper}>
            <Icon className={classes.splashIcon} color="inherit">
              <NoteAddIcon className={classes.linkIcon}/>      
            </Icon>
            <span className={classes.splashIconText}> New Test</span>
          </Paper>
        </Link>
      </Grid>

      <Grid item xs={12} lg={3}>
        <Link to="/uploads">
          <Paper className={classes.splashPaper}>
            <Icon className={classes.splashIcon} color="inherit">
              <BusinessCenterIcon className={classes.linkIcon}/>      
            </Icon>
            <span className={classes.splashIconText}> Manage Uploads</span>
          </Paper>
        </Link>
      </Grid>

      <Grid item xs={12} lg={3}>
          <Link to="/results">
          <Paper className={classes.splashPaper}>
            <Icon className={classes.splashIcon} color="inherit">
              <AssignmentIcon className={classes.linkIcon}/>      
            </Icon>
            <span className={classes.splashIconText}> Test Results</span>
          </Paper>
        </Link>
      </Grid>

      <Grid item xs={12} lg={3}>
        <Link to="/metrics">
          <Paper className={classes.splashPaper}>
            <Icon className={classes.splashIcon} color="inherit">
              <TrendingUpIcon className={classes.linkIcon}/>      
            </Icon>
            <span className={classes.splashIconText}> LucidLab Status</span>
          </Paper>
        </Link>

      </Grid>
    </Grid>
    <div className={'hp-teamMembers'}>
      <TeamMember member={constants.TEAM.SAM}/>
      <TeamMember member={constants.TEAM.JAY}/>
      <TeamMember member={constants.TEAM.KYLE}/>
      <TeamMember member={constants.TEAM.WILL}/>
      <TeamMember member={constants.TEAM.YANNIS}/>
      <TeamMember member={constants.TEAM.SCOTT}/>
    </div>

  </Container>
  );
}

export default HomePage;