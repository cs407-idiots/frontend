/*
    StatusPage.js

    This file serves the tab panel for LucidLab's status pages, namely the Scheduled Tests page, the
    Mote Availability metrics and the connected IoT devices.
 */

import React from 'react';
import './App.css';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import {withStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TabPanel from './components/TabPanel';
import MoteAvailability from './components/MoteAvailability';
import ScheduledTests from './components/ScheduledTests';
import IoTDevices from './components/IoTDevices';
import PropTypes from 'prop-types';

const useStyles = theme => ({
    root: {
        marginBottom: "5em",
    },
    title: {
        fontSize: "2rem",
        textAlign: "left",
    },
    tabBar: {
        backgroundColor: "#6BC193",
        color: "white",
        fontWeight: "bold",
    },
    tabTitle: {
        paddingLeft: "4em",
        paddingRight: "4em",
        fontSize: "1rem",
        textTransform: "inherit",
        opacity: "1.0!important",
    }
});


function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

class StatusPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 1
        }
    }

    handleChange(event, newValue) {
        this.setState({ value: newValue });
    }

    render() {
        const { classes } = this.props;

        return (
            <Container className={classes.root} maxWidth="lg">
                <AppBar className={classes.tabBar} position="static" color="inherit">
                    <Tabs className={classes.tabBar} value={this.state.value} onChange={this.handleChange.bind(this)} aria-label="simple tabs example" centered>
                        <Tab label="LucidLab Status:" disabled className={classes.tabTitle} />
                        <Tab label="Scheduled Tests" {...a11yProps(1)} />
                        <Tab label="Mote Availability" {...a11yProps(2)} />
                        <Tab label="IoT Devices" {...a11yProps(3)} />
                    </Tabs>
                </AppBar>
                <TabPanel className={classes.tab} value={this.state.value} index={1}>
                    <ScheduledTests userID={this.props.userID} />
                </TabPanel>
                <TabPanel className={classes.tab} value={this.state.value} index={2}>
                    <MoteAvailability userID={this.props.userID} />
                </TabPanel>
                <TabPanel className={classes.tab} value={this.state.value} index={3}>
                    <IoTDevices userID={this.props.userID} />
                </TabPanel>
            </Container>
        );
    }
}

// Higher-Order component.
StatusPage.propTypes = {
    classes: PropTypes.object.isRequired,
    userID: PropTypes.number.isRequired
};

export default withStyles(useStyles)(StatusPage);