/*
    Endpoint.js

    This file acts as a wrapper for Axios sending and receiving requests from a Python Flask endpoint
    in the back-end. Facilitates basic POST and GET requests, as well as asynchronous GET requests that
    enable multiple GETs to be performed when a page first loads.
 */

import * as endpoints from './constants/endpoints';

const axios = require('axios').default;
const hostname = endpoints.hostName;

export default class Endpoint {
    constructor(url, successCallback, errorCallback) {
        this.url = hostname + url; // URL of the endpoint.
        this.successCallback = successCallback; // Function to execute on success.
        this.errorCallback = errorCallback; // Function to execute on error.
        this.logErrors = true; // Show errors on the console.
    }
    
    // Send a POST request to the endpoint with a file attached.
    uploadFile(data) {
      axios.post(this.url, data, {
        headers: {
          "Content-Type": "multipart/form-data",
        }
      })
      .then(response => {
        // Call the provided success callback function.
        this.successCallback(response);
      })
      .catch(error => {
        let msg;
        // Request made, server responded with status code.
        if (error.response) {
          // Internal Server Error.
          if (error.response.status === 500) {
            msg = "[" + error.response.status + "] Internal Server Error.";
          } 
          else {
            msg = "[" + error.response.status + "] " + error.response.data;
          }
        }
        // Request made, no response from server.
        else if (error.request) {
          msg = "No response from the server was received.";
        }
        // Other error.
        else {
          msg = "Unexpected error.";
        }

        if (this.logErrors) {
          console.log(msg);
        }

        // Call the provided error callback function.
        this.errorCallback(error, msg);
      });
    }

    // Send a POST request to the endpoint.
    post(data) {
      axios.post(this.url, data)
      .then(response => {
        // Call the provided success callback function.
        this.successCallback(response);
      })
      .catch(error => {
        let msg;
        // Request made, server responded with status code.
        if (error.response) {
          // Internal Server Error.
          if (error.response.status === 500) {
            msg = "[" + error.response.status + "] Internal Server Error.";
          } 
          else {
            msg = "[" + error.response.status + "] " + error.response.data;
          }
        }
        // Request made, no response from server.
        else if (error.request) {
          msg = "No response from the server was received.";
        }
        // Other error.
        else {
          msg = "Unexpected error.";
        }

        if (this.logErrors) {
          console.log(msg);
        }

        // Call the provided error callback function.
        this.errorCallback(error, msg);
        });
    }

    // Send a GET request to the endpoint.
    get(data) {
      axios.get(this.url, {
        params: data
      })
      .then(response => {
        // Call the provided success callback function.
        return this.successCallback(response);
      })
      .catch(error => {
        let msg;
        // Request made, server responded with status code.
        if (error.response) {
          // Internal Server Error.
          if (error.response.status === 500) {
            msg = "[" + error.response.status + "] Internal Server Error.";
          } 
          else {
            msg = "[" + error.response.status + "] " + error.response.data;
          }
        }
        // Request made, no response from server.
        else if (error.request) {
          msg = "No response from the server was received.";
        }
        // Other error.
        else {
            console.log(error);
            msg = "Unexpected error.";
        }

        if (this.logErrors) {
            console.log(msg);
        }

        // Call the provided error callback function.
        this.errorCallback(error, msg);
      });
    }

    // Send an Asynchronous GET request to the endpoint.
    async asyncGet(data) {
        return await axios.get(this.url, {
            params: data
        })
            .then(response => {
                // Call the provided success callback function.
                return this.successCallback(response);
            })
            .catch(error => {
                let msg;
                // Request made, server responded with status code.
                if (error.response) {
                    // Internal Server Error.
                    if (error.response.status === 500) {
                        msg = "[" + error.response.status + "] Internal Server Error.";
                    }
                    else {
                        msg = "[" + error.response.status + "] " + error.response.data;
                    }
                }
                // Request made, no response from server.
                else if (error.request) {
                    msg = "No response from the server was received.";
                }
                // Other error.
                else {
                    msg = "Unexpected error.";
                }

                if (this.logErrors) {
                    console.log(msg);
                }

                // Call the provided error callback function.
                this.errorCallback(error, msg);
            });
    }
}