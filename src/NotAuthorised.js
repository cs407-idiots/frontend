/*
    NotAuthorised.js

    A basic 401: Not Authorised error to display to the user if they try to access a page on the site
    without proper authentication.
    Redirects to the homepage if the user signs in while viewing the page.
 */

import React from 'react';
import './App.css';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {Link, Redirect} from 'react-router-dom';

const useStyles = theme => ({
  root: {
    marginBottom: "5em",
  },
  title: {
    marginTop: "2em",
    marginBottom: "2em",      
  },
  desc: {}
});

class NotAuthorised extends React.Component {

  render() {
    const { classes } = this.props;

    // Redirects to homepage if user signs in.
    if (this.props.signedIn) {
      return (
        <Redirect to={"/"} />
      )
    }

    return (
      <Container className={classes.root} maxWidth="lg">
        <Typography className={classes.title} variant="h2" component="h1">401 Not Authorised</Typography>
        <Typography className={classes.desc} variant="h5" component="h2">You are not authorised to view this page. Please log in or sign up.</Typography>
        <Typography variant="h6" className={classes.title}>
            <Link className={classes.link} variant="inherit" to="/">{"Click here to return to the homepage."}</Link>
          </Typography>
      </Container>
    );
  }
}

// Higher-Order component.
NotAuthorised.propTypes = {
    classes: PropTypes.object.isRequired,
    signedIn:  PropTypes.bool.isRequired,
};  

export default withStyles(useStyles)(NotAuthorised);