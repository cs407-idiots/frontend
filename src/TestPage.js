/*
    TestPage.js

    This file serves the tab panel for LucidLab's test configutation pages, namely the Upload Test page
    and the online test configurator, that enables the user to create a configuration in an interactive
    form.

    The tab panels are also responsible for passing the currently signed in user ID to the components,
    for use in requests to the back-end server.
 */

import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import UploadTest from './components/UploadTest';
import CreateTest from './components/CreateTest';
import Container from '@material-ui/core/Container';
import TabPanel from './components/TabPanel';
import PropTypes from 'prop-types';

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = theme => ({
    root: {
        flexGrow: 1,
    },
    tab: {
        paddingTop: "2em",
    },
    tabBar: {
        backgroundColor: "#6BC193",
        color: "white",
        fontWeight: "bold",
    },
    tabTitle: {
        paddingLeft: "4em",
        paddingRight: "4em",
        fontSize: "1rem",
        textTransform: "inherit",
        opacity: "1.0!important",
        boxShadow: "none",
    }
});


class TestPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 1
        }
    }

    handleChange(event, newValue) {
        this.setState({ value: newValue });
    };

    render() {
        const { classes } = this.props;

        return (
            <Container className={classes.root} maxWidth="lg">
                <AppBar className={classes.tabBar} position="static" color="inherit">
                    <Tabs value={this.state.value} onChange={this.handleChange.bind(this)} aria-label="simple tabs example" centered>
                        <Tab className={classes.tabTitle} disabled label="New Test Config:" />
                        <Tab label="Upload Test" {...a11yProps(1)} />
                        <Tab label="Create Test" {...a11yProps(2)} />
                    </Tabs>
                </AppBar>
                <TabPanel className={classes.tab} value={this.state.value} index={1}>
                    <UploadTest userID={this.props.userID} />
                </TabPanel>
                <TabPanel className={classes.tab} value={this.state.value} index={2}>
                    <CreateTest userID={this.props.userID} />
                </TabPanel>
            </Container>
        );
    }
}

// Higher-Order component.
TestPage.propTypes = {
    classes: PropTypes.object.isRequired,
    userID: PropTypes.number.isRequired
};

export default withStyles(useStyles)(TestPage);