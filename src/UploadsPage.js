/*
    UploadsPage.js

    This file serves the tab panel for LucidLab's supplementary uploads, namely the Upload Image page,
    the Manage Images page, the Upload IoT Controller Page and the Manage IoT Controllers page.

    The tab panels are also responsible for passing the currently signed in user ID to the individual
    components, for use in requests to the back-end server.
 */

import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Container from '@material-ui/core/Container';
import TabPanel from './components/TabPanel';
import ImageList from './components/ImageList';
import UploadImage from './components/UploadImage';
import IoTControllerList from "./components/IoTControllerList";
import UploadIoTController from "./components/UploadIoTController";
import PropTypes from "prop-types";

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = theme => ({
  root: {
    flexGrow: 1,
  },
  tabBar: {
    backgroundColor: "#6BC193",
    color: "white",
    fontWeight: "bold",
  },
  tabTitle: {
    paddingLeft: "4em",
    paddingRight: "4em",
    fontSize: "1rem",
    textTransform: "inherit",
    opacity: "1.0!important",
  }
});

class UploadsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 1
        }
    }

    handleChange = (event, newValue) => {
        this.setState({ value: newValue });
    };

    changeTab = (newValue) => {
        this.setState({ value: newValue });
    };

    render() {
        const {classes} = this.props;
        const changeTab = this.changeTab.bind(this);

        return (
            <Container className={classes.root} maxWidth="lg">
                <AppBar className={classes.tabBar} position="static" color="inherit">
                    <Tabs value={this.state.value} onChange={this.handleChange} centered>
                        <Tab className={classes.tabTitle} disabled label="Manage Uploads:"/>
                        <Tab label="Upload Image" {...a11yProps(1)} />
                        <Tab label="Manage Images" {...a11yProps(2)} />
                        <Tab label="Upload IoT Controller" {...a11yProps(3)} />
                        <Tab label="Manage IoT Controllers" {...a11yProps(4)} />
                    </Tabs>
                </AppBar>
                <TabPanel className={classes.tab} value={this.state.value} index={1}>
                    <UploadImage userID={this.props.userID} switchTab={changeTab}/>
                </TabPanel>
                <TabPanel className={classes.tab} value={this.state.value} index={2}>
                    <ImageList userID={this.props.userID}/>
                </TabPanel>
                <TabPanel className={classes.tab} value={this.state.value} index={3}>
                    <UploadIoTController userID={this.props.userID} switchTab={changeTab}/>
                </TabPanel>
                <TabPanel className={classes.tab} value={this.state.value} index={4}>
                    <IoTControllerList userID={this.props.userID}/>
                </TabPanel>
            </Container>
        );
    }
}

// Higher-Order component.
UploadsPage.propTypes = {
    classes:    PropTypes.object.isRequired,
    userID:     PropTypes.number.isRequired
};

export default withStyles(useStyles)(UploadsPage);