/*
    constants/index.js

    This is the core constants file used across the front-end system, allowing changes to be made to
    variable options without having to find the relevant section in the codebase. Aids in code
    maintainability, as future developers can change these options and be assured that the relevant areas
    in the code are affected, without having to find all of their individual usages.
 */

export const TEAM = {
    "SAM": {
        "name": "Samuel Bor",
        "title": "Front-End Developer",
        "img": "img/team/sam.png"
    },
    "KYLE": {
        "name": "Kyle Gough",
        "title": "Front-End Developer",
        "img": "img/team/kyle.png"
    },
    "JAY": {
        "name": "Jay Carder",
        "title": "Project Manager & IoT Developer",
        "img": "img/team/jay.png"
    },
    "WILL": {
        "name": "Will Osborne",
        "title": "Network Engineer",
        "img": "img/team/will.png"
    },
    "YANNIS": {
        "name": "Yannis Patas",
        "title": "IoT Developer",
        "img": "img/team/yannis.png"
    },
    "SCOTT": {
        "name": "Scott Pickering",
        "title": "Network Engineer",
        "img": "img/team/scott.png"
    }
};

export const image_os = [
    {key: "contiki", value: 'Contiki'},
    {key: 'riot', value: 'RIOT'}
];

export const image_platform = [
    { key: "zolertia", value: 'Zolertia Re-mote rev-b' },
    { key: 'telosb', value: 'TelosB' }
];

export const accepted_image_file_types = [
    '.bin',
    '.hex',
    '.ihex',
    '.elf'
];

export const accepted_controller_file_types = [
    '.py'
];

export const upload_page_tab_index = {
    "Upload-Image": 1,
    "Manage-Images": 2,
    "Upload-IoT-Controller": 3,
    "Manage-IoT-Controllers": 4
};

export const mote_timeout_status = "Connection timeout";

export const cca_bar_chart_colour = "#4BA173";