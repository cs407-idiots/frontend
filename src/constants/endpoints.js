/*
    constants/endpoints.js

    This constants file holds the endpoint routes offered by the back-end testbed server, and maps
    them onto readable variable names. Similar to the core constants file, this means that future
    developers can change/add to the routes in this file and the change will be propagated to every
    usage of the endpoint.
 */

// Hostname of the testbed server
export const hostName = "http://" + window.location.hostname + ":8080";

// Testbed endpoints with which to connect the front end components to
// Test Configurations
export const UploadConfig = "/upload-configuration";
export const DownloadConfig = "/get-test-config-file";
export const GetConfigInfo = "/get-test-info";

// Images
export const UploadImage = "/upload-image";
export const GetImages = "/user-images";
export const DeleteImage = "/delete-image";
export const DownloadImage = "/download-image";

// IoT Controllers
export const UploadController = "/upload-iot-controller";
export const DownloadController = "/download-iot-controller";
export const GetControllers = "/user-iot-controllers";
export const DeleteController = "/delete-iot-controller";

// Jobs
export const GetCompletedJobs = "/get-user-completed-jobs";
export const GetScheduledJobs = "/get-user-scheduled-jobs";
export const GetAllJobs = "/get-all-jobs";
export const DeleteJob = "/delete-job";
export const DownloadResult = "/get-job-results-csv";

// Login System
export const CreateUser = "/create-user";
export const TestLogin = "/test-login";

// Misc
export const GetAvailableIoTDevices = "/get-iot-things";
export const GetMotes = "/mote-availability";
export const GetRSSIValues = "/get-rssi-map";
export const GetCCAValues = "/get-cca-data";
export const GetNoiseFloorValues = "/get-noise-floor-data";